<?php
	session_start();
	if(isset($_SESSION['username'])){
		include '../config/koneksi.php';

		$sql_notif = mysql_query("SELECT * FROM notifikasi_permintaan_bb JOIN permintaan_bahan_baku ON notifikasi_permintaan_bb.id_permintaan = permintaan_bahan_baku.id_permintaan JOIN bahan_baku ON bahan_baku.kode_bahan_baku = permintaan_bahan_baku.kode_bahan_baku WHERE notifikasi_permintaan_bb.status_notif = 'belum approve' ORDER BY notifikasi_permintaan_bb.tanggal_notif DESC ");
		$data_notif = mysql_num_rows($sql_notif);

		$view_all = mysql_fetch_array(mysql_query("SELECT * FROM notifikasi_permintaan_bb WHERE status_notif = 'belum approve' ORDER BY tanggal_notif DESC "));
		$setting_data = mysql_fetch_array(mysql_query("SELECT pengaturan_suara FROM pengaturan_notifikasi WHERE kode = '002' "));
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PT.AFIT | Gudang</title>
  <link rel="shortcut icon" href="https://andriafesyenindonesiatekstil.co.id/wp-content/uploads/2021/08/favicon-32x32-3.png" type="image/x-icon" />
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../template/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="../template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../template/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../template/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="../template/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../template/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../template/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../template/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../template/plugins/summernote/summernote-bs4.min.css">

  <!-- toastr -->
  <link href="../toastr/build/toastr.css" rel="stylesheet"/>

  <!-- DataTables -->
  <link rel="stylesheet" href="../template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../template/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../template/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="../template/dist/img/afit.png" alt="PT.AFIT" height="60" width="220">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-fixed navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index.php" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge"><?= $data_notif ?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><?= $data_notif ?> Notifikasi</span>
          <div class="dropdown-divider"></div>
          <?php while($list_notif = mysql_fetch_array($sql_notif)){ ?>
          <a href="?page=detail_notifikasi&id_notifikasi=<?php echo $list_notif['id_notifikasi']; ?>" class="dropdown-item">
            <i class="fas fa-cubes mr-2"></i> Permintaan <?php echo $list_notif['nama_bahan_baku']; ?>
            <span class="float-right text-muted text-sm"><?php echo $list_notif['tanggal_notif']; ?></span>
          </a>
          <br>
          <div class="dropdown-divider"></div>
          <?php } ?>
          <a href="?page=all_notifikasi" class="dropdown-item dropdown-footer">Lihat Semua</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
	  	<a href="#" data-toggle="modal" data-target="#alert-log" class="nav-link">
        	<i class="fas fa-power-off"></i> Logout
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link" style="margin-left: 40px;">
      <span class="brand-text font-weight-light">PT.AFIT - Gudang</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../template/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $_SESSION['nama'] ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <?php include('nav-menu.php') ?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <?php
	$page = $_GET['page'];
	if ($page=="") {
		include 'beranda.php';
	}else{
		switch ($page) {
			case 'beranda':
				include 'beranda.php';
				break;
			// ============== Module data master ============ //
			case 'master_bahan_baku':
				include 'module/data_master_bahan_baku/master-bb.php';
				break;
			case 'master_supplier':
				include 'module/data_master/master_supplier.php';
				break;
			case 'master_pelanggan':
				include 'module/data_master/master_pelanggan.php';
				break;
			// ============== End module data master ============ //

			//**============== Module Profil ============**//
			case 'lihat_profil':
				include 'module/profil/lihat_profil.php';
				break;
			case 'edit_profil':
				include 'module/profil/edit_profil.php';
				break;
			case 'ganti_password':
				include 'module/profil/ganti_password.php';
				break;
			case 'pengaturan_notifikasi':
				include 'module/profil/pengaturan_notifikasi.php';
				break;
			//**============== End Module Profil ============**//
			//**============== Module master bahan baku ============**//
		
			case 'lihat_persediaan_bahan_baku':
				include 'module/data_master_bahan_baku/lihat_persediaan_bahan_baku.php';
				break;
			//**============== End Module master bahan baku ============**//
			//**============== Module pemesanan bahan baku ============**//
			case 'form_pemesanan':
				include 'module/pemesanan_bb/form_pemesanan.php';
				break;
			case 'lihat_pemesanan':
				include 'module/pemesanan_bb/lihat_pemesanan.php';
				break;
			case 'detail_pemesanan':
				include 'module/pemesanan_bb/detail_pemesanan.php';
				break;
			//**============== End Module pemesanan ============**//

			case 'pengeluaran_bahan_baku':
				include 'module/pengeluaran_bb/pengeluaran_bahan_baku.php';
				break;
			case 'detail_notifikasi':
				include 'module/pengeluaran_bb/detail_notifikasi.php';
				break;
			case 'all_notifikasi':
				include 'module/pengeluaran_bb/all_notifikasi.php';
				break;

      //**============== Module Data Bahan/Bahan Baku ============**//
      case 'master-bb':
          include 'module/data_master_bahan_baku/master-bb.php';
          break;
        case 'bb-sm':
          include 'module/data_master_bahan_baku/bb-sm.php';
          break;
        case 'bb-sk':
          include 'module/data_master_bahan_baku/bb-sk.php';
          break;
        case 'bb-used':
          include 'module/data_master_bahan_baku/bb-used.php';
          break;

        case 'kirim-bintaro':
          include 'module/data_master_bahan_baku/kirim-bintaro.php';
          break;

        case 'keluar-bahan':
          include 'module/data_master_bahan_baku/keluar-bahan.php';
          break;  

        case 'return-bb':
          include 'module/data_master_bahan_baku/return-bb.php';
          break;    

        //**============== Module bahan jadi ============**//
          
        case 'jenis-produk':
          include 'module/bahan-jadi/jenis_produk.php';
          break;

        case 'produk':
          include 'module/bahan-jadi/produk.php';
          break;

        case 'produk-masuk':
          include 'module/bahan-jadi/produk-masuk.php';
          break;

        case 'produk-keluar':
          include 'module/bahan-jadi/produk-keluar.php';
          break;

        case 'produk-edit':
          include 'module/bahan-jadi/produk-edit.php';
          break;

        case 'produk-sm':
          include 'module/bahan-jadi/produk-sm.php';
          break;

        case 'produk-sk':
          include 'module/bahan-jadi/produk-sk.php';
          break;

      //**============== Module pengambilan ============**//
          
      case 'pengambilan-online':
        include 'module/pengambilan/pengambilan-online.php';
        break;

        case 'pengambilan-barang':
          include 'module/pengambilan/pengambilan-barang.php';
          break;

      case 'pengambilan-umum':
        include 'module/pengambilan/pengambilan-umum.php';
        break;

      case 'pengambilan-cipulir':
        include 'module/pengambilan/pengambilan-cipulir.php';
        break;

      case 'laporan-pengambilan':
        include 'module/pengambilan/laporan-pengambilan.php';
        break;

      //**============== Module Surat Jalan ============**//
          
      case 'form-surat-jalan':
        include 'module/surat-jalan/form-surat-jalan.php';
        break;
  
        case 'edit-surat-jalan':
          include 'module/surat-jalan/edit-surat-jalan.php';
          break;

        case 'report-surat-jalan':
          include 'module/surat-jalan/report-surat-jalan.php';
          break;

      //**============== Module Surat Jalan ============**//
          
      case 'report-pengambilan':
        include 'module/report/report-pengambilan.php';
        break;
  
      case 'report-sm':
        include 'module/report/report-sm.php';
        break;

      case 'report-sk':
        include 'module/report/report-sk.php';
        break;

      case 'report-bm':
        include 'module/report/report-bm.php';
        break;

      case 'report-bk':
        include 'module/report/report-bk.php';
        break;
          

			//**============== Module chatting ============**//
			case 'pesan_terkirim':
				include 'module/chatting/pesan_terkirim.php';
				break;
			case 'detail_chatting':
				include 'module/chatting/detail_chatting.php';
				break;
			//**============== End Module chatting ============**//

			//**============== Module Penjualan ============**//
			case 'input_penjualan':
				include 'module/penjualan/input_penjualan.php';
				break;
			case 'data_penjualan':
				include 'module/penjualan/data_penjualan.php';
				break;
			case 'detail_barang':
				include 'module/penjualan/detail_barang.php';
				break;
			//**============== End Module Penjualan ============**//

			case 'data_barang_jadi':
				include 'module/barang_jadi/data_barang_jadi.php';
				break;

		}
	}
?>
  </div>

  <!-- alert logout -->
  <div class="modal fade bs-example-modal-md" id="alert-log" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-md" style="margin-top:200px;" >
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel"><span class="fa fa-lock"></span>&nbsp;&nbsp;Logout Sistem</h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modal-body" >
			<h4>Apakah anda ingin keluar dari sistem ini ?</h4>
			</div>
			<div class="modal-footer">
			<a class="btn btn-default" data-dismiss="modal">Batalkan</a>
			<a href="../config/logout.php" class="btn btn-danger"><i class="fa fa-check fa"></i>&nbsp;Ya, Logout</a>
			</div>
		</div>
	</div>
</div>
<!-- End alert logout -->

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; <?= date('Y') ?> <a href="index.php">PT.ANDRIA FESYEN INDONESIA TEKSTIL</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../template/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../template/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- daterangepicker -->
<script src="../template/plugins/moment/moment.min.js"></script>
<script src="../template/plugins/daterangepicker/daterangepicker.js"></script>
<!-- overlayScrollbars -->
<script src="../template/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../template/dist/js/adminlte.js"></script>
<!-- DataTables  & Plugins -->
<script src="../template/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../template/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../template/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../template/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../template/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../template/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="../template/plugins/select2/js/select2.full.min.js"></script>
<script src="../template/plugins/jszip/jszip.min.js"></script>
<script src="../template/plugins/pdfmake/vfs_fonts.js"></script>
<script src="../template/plugins/pdfmake/pdfmake.min.js"></script>
<script src="../template/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="../template/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="../template/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- toastr -->
<script src="../toastr/toastr.js"></script>
<script>
  $(function () {
    $('.select2').select2();
    $('#example2').DataTable({
      "paging": true,
      "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "pageLength": 10,
    });

    $("#example1").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
      "autoWidth": false,
      "buttons": ["print",
      {
          extend: 'excelHtml5',
          text: 'Export Excel',
          className: 'btn btn-success',
          autoFilter: true,
          sheetName: 'Exported data',
          title:'Data Produk',
          exportOptions: {
                    columns: ':visible'
                }
      },
      {
          extend: 'colvis',
          text: 'Column Setting',
          className: 'btn btn-danger',
          collectionLayout: 'responsive columns',
      }],
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    $('#example22').DataTable({
      "paging": true,
      "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });

    

    
  });
</script>
</body>
</html>
<?php
	}else{
		header("location:../login/");
	}
?>