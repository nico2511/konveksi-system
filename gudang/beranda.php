<?php
  include '../config/koneksi.php';
  $wulan_saiki = date('m');
  $ws = date('m');
  Switch ($ws){
    case 1 : $ws="Januari";
        Break;
    case 2 : $ws="Februari";
        Break;
    case 3 : $ws="Maret";
        Break;
    case 4 : $ws="April";
        Break;
    case 5 : $ws="Mei";
        Break;
    case 6 : $ws="Juni";
        Break;
    case 7 : $ws="Juli";
        Break;
    case 8 : $ws="Agustus";
        Break;
    case 9 : $ws="September";
        Break;
    case 10 : $ws="Oktober";
        Break;
    case 11 : $ws="November";
        Break;
    case 12 : $ws="Desember";
        Break;
    }

  $tahun_saiki = date('Y');
  $saiki       = date('Y-m-d');
  $or_period   = $ws.' '.$tahun_saiki;
  $sql = mysql_fetch_array(mysql_query("SELECT * from periode where month(mask) = '$wulan_saiki' AND year(mask) = '$tahun_saiki' "));
  if(!$sql){
    $query1 = mysql_query("INSERT into periode values(NULL,'$or_period','$saiki')") or die('Gagal');
  }
?>
<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"> Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>Bahan Baku</h3>

                <p>Stock Bahan Masuk</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="?page=bb-sm" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <h3>Bahan Baku</h3>

                <p>Stock Bahan Keluar</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="?page=bb-sk" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>Produk YOVIS</h3>

                <p>Stock Produk Masuk</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="?page=produk-sm" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>Produk YOVIS</h3>

                <p>Stock Produk Keluar</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="?page=produk-sk" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row"></div>
<!-- BAR CHART -->
<div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    