    <?php $p = $_GET['page']; ?>
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="index.php" class="nav-link active">
                    <i class="fas fa-home nav-icon"></i>
                    <p>Beranda</p>
                </a>
            </li>
            <?php if($_SESSION['sublevel'] == 'admin'){  ?>
            <li class="nav-item <?php if($p == 'master_supplier' || $p == 'master_pelanggan'){ echo 'menu-open'; } ?>">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-server"></i>
                    <p>Data Master<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <!-- <li class="nav-item">
                        <a class="nav-link <?php if($p == 'master_bahan_baku'){ echo 'active'; } ?>" href="?page=master_bahan_baku">Bahan Baku</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'master_supplier'){ echo 'active'; } ?>" href="?page=master_supplier">Supplier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'master_pelanggan'){ echo 'active'; } ?>" href="?page=master_pelanggan">Pelanggan</a>
                    </li>
                </ul>
            </li>
            <?php }
            if($_SESSION['sublevel'] == 'admin'){  ?>
            
            <li class="nav-item <?php if($p == 'form_pemesanan' || $p == 'lihat_pemesanan'){ echo 'menu-open'; } ?>">
            <a href="#" class="nav-link">
                    <i class="nav-icon fab fa-wpforms"></i>
                    <p>Form Bahan Baku<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'form_pemesanan'){ echo 'active'; } ?>" href="?page=form_pemesanan">Penerimaan BB</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'lihat_pemesanan'){ echo 'active'; } ?>" href="?page=lihat_pemesanan">Detail Penerimaan</a>
                    </li>
                </ul>
            </li>
            <?php }
            if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin'){ ?>
            <li class="nav-item <?php if($p == 'master_bahan_baku' || $p == 'bb-sm' || $p == 'bb-sk' || $p == 'bb-used' || $p == 'return-bb'){ echo 'menu-open'; } ?>">
                <a href="#" class="nav-link">
                    <i class="fas fa-cubes nav-icon"></i>
                    <p>Bahan Baku<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'master_bahan_baku'){ echo 'active'; } ?>" href="?page=master_bahan_baku">Data Master</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'bb-sm'){ echo 'active'; } ?>" href="?page=bb-sm">Stock Produk Masuk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'bb-sk'){ echo 'active'; } ?>" href="?page=bb-sk">Stok Produk Keluar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'bb-used'){ echo 'active'; } ?>" href="?page=bb-used">Bahan Terpakai</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'return-bb'){ echo 'active'; } ?>" href="?page=return-bb">Return Bahan Baku</a>
                    </li>
                </ul>
            </li>
            <?php } 
            if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin' || $_SESSION['sublevel'] == 'staff gudang'){  ?>
            <li class="nav-item <?php if($p == 'jenis-produk' || $p == 'produk' || $p == 'produk-sk' || $p == 'produk-sm'){ echo 'menu-open'; } ?>">
                <a href="#" class="nav-link">
                    <i class="fas fa-layer-group nav-icon"></i>
                    <p>Produk Yovis<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'jenis-produk'){ echo 'active'; } ?>" href="?page=jenis-produk">Jenis Produk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'produk'){ echo 'active'; } ?>" href="?page=produk">Data Produk</a>
                    </li>
                    <?php if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin'){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'produk-sm'){ echo 'active'; } ?>" href="?page=produk-sm">Stock Produk Masuk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'produk-sk'){ echo 'active'; } ?>" href="?page=produk-sk">Stok Produk Keluar</a>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            <?php } 
            if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin' || $_SESSION['sublevel'] == 'staff gudang'){ ?>
            <li class="nav-item <?php if($p == 'pengambilan-barang' || $p == 'pengambilan-online' || $p == 'pengambilan-umum' || $p == 'pengambilan-cipulir'){ echo 'menu-open'; } ?>">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-truck-loading"></i>
                    <p>Pengambilan<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <?php if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin' || $_SESSION['sublevel'] == 'staff gudang'  ){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'pengambilan-barang'){ echo 'active'; } ?>" href="?page=pengambilan-barang">Pengambilan Barang</a>
                    </li>
                    <?php } ?>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'pengambilan-online'){ echo 'active'; } ?>" href="?page=pengambilan-online">Pengambilan Online</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'pengambilan-umum'){ echo 'active'; } ?>" href="?page=pengambilan-umum">Pengambilan Umum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'pengambilan-cipulir'){ echo 'active'; } ?>" href="?page=pengambilan-cipulir">Pengambilan Cipulir</a>
                    </li>
                </ul>
            </li>
            <?php } 
            if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin'){ ?>
            <li class="nav-item <?php if($p == 'form-surat-jalan' || $p == 'report-surat-jalan'){ echo 'menu-open'; } ?>">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-envelope-open-text"></i>
                    <p>Surat Jalan<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'form-surat-jalan'){ echo 'active'; } ?>" href="?page=form-surat-jalan">Form Surat Jalan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'report-surat-jalan'){ echo 'active'; } ?>" href="?page=report-surat-jalan">Laporan Surat Jalan</a>
                    </li>
                </ul>
            </li>
            <?php } 
            if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin'){ ?>
            <li class="nav-item <?php if($p == 'report-pengambilan' || $p == 'report-sm' || $p == 'report-sk' || $p == 'report-bm' || $p == 'report-bk'){ echo 'menu-open'; } ?>">
                <a href="#" class="nav-link">
                    <i class="fas fa-file-invoice nav-icon"></i>
                    <p>Laporan<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <?php if($_SESSION['sublevel'] == 'admin' || $_SESSION['sublevel'] == 'staff admin'){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'report-pengambilan'){ echo 'active'; } ?>" href="?page=report-pengambilan">Pengambilan Sesuai Devisi</a>
                    </li>
                    <?php }
                    if($_SESSION['sublevel'] == 'admin'){ ?>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'report-sm'){ echo 'active'; } ?>" href="?page=report-sm">Stock Masuk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'report-sk'){ echo 'active'; } ?>" href="?page=report-sk">Stock Keluar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'report-bm'){ echo 'active'; } ?>" href="?page=report-bm">Bahan Masuk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if($p == 'report-bk'){ echo 'active'; } ?>" href="?page=report-bk">Bahan Keluar</a>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </nav>