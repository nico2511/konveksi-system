<?php
    include '../config/koneksi.php';
    session_start();
    $id     = $_GET['id'];
    $master = mysql_fetch_array(mysql_query("SELECT * from sj_master where id = '$id'"));
    $detail = mysql_query("SELECT * from sj_detail where sj_id = '$id'");
    $hit   =  mysql_fetch_array(mysql_query("SELECT count(id) as hit from sj_detail where sj_id = '$id'"));    
    
?>

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Surat Jalan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Form Surat Jalan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form role="form-horizontal" action="module/surat-jalan/proses/edit-sj.php" method="POST" onsubmit="return validasi()">
                                    <input type="hidden" name="lastRow" id="lastRow" value="<?= $hit['hit'] ?>">
                                    <div class="form-group">
                                        <label>Kode DO</label>
                                        <input type="hidden" name="id" id="id" value="<?php echo $master['id']; ?>" readonly class="form-control" />
                                        <input type="text" name="kode" id="kode" value="<?php echo $master['kode']; ?>" readonly class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Pesan</label>
                                        <input type="date" name="tgl" id="tgl" class="form-control" value="<?= $master['tgl'] ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Input By</label>
                                        <input type="text" name="input_by" id="input_by" class="form-control" value="<?= $master['created_by'] ?>" readonly/>
                                    </div>
                                    <table id="table_sj" class="table table-bordered table-striped table-sm">
                                        <thead>
                                            <tr style="text-align: center;">
                                                <th>No</th>
                                                <th>No Seri</th>
                                                <th>Nama Bahan</th>
                                                <th>Warna Bahan</th>
                                                <th>Jumlah</th>
                                                <th>Roll</th>
                                                <th>Kg</th>
                                                <th>Keterangan</th>
                                                <th><a class="btn btn-sm btn-success" id="btn_add"><i class="fa fa-plus"></i></a></th>
                                            </tr>
                                        </thead>
                                        <tbody id="body">
                                            <?php $r = 1; $no = 1; while($b = mysql_fetch_array($detail)){ ?>
                                            <tr id="r[<?= $r ?>]">
                                                <td><?= $no ?></td>
                                                <input type="hidden" name="id_detail[]" id="id_detail[]" value="<?= $b['id'] ?>">
                                                <td><input type="text" name="no_seri[]" id="no_seri[]" class="form-control" value="<?= $b['no_seri'] ?>"></td>
                                                <td><input type="text" name="nama_bahan[]" id="nama_bahan[]" class="form-control" value="<?= $b['nama_bahan'] ?>"></td>
                                                <td><input type="text" name="warna_bahan[]" id="warna_bahan[]" class="form-control" value="<?= $b['warna_bahan'] ?>"></td>
                                                <td><input type="text" name="jumlah[]" id="jumlah[]" class="form-control" value="<?= $b['jumlah'] ?>"></td>
                                                <td><input type="text" name="roll[]" id="roll[]" class="form-control" value="<?= $b['roll'] ?>"></td>
                                                <td><input type="text" name="kg[]" id="kg[]" class="form-control" value="<?= $b['kg'] ?>"></td>
                                                <td><input type="text" name="keterangan[]" id="keterangan[]" class="form-control" value="<?= $b['keterangan'] ?>"></td>
                                                <td><a href="#modal-hapus<?= $b['id'] ?>" data-toggle="modal" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a></td>
                                            </tr>

                                            <div class="modal fade bs-example-modal-md" id="modal-hapus<?= $b['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                                <div class="modal-dialog modal-md" style="margin-top:50px;" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        </div>
                                                        <div class="modal-body" >
                                                            Apakah kamu yakin ingin menghapus detail Surat Jalan dengan No Seri <b><?= $b['no_seri'] ?> ?</b>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                            <a class="btn btn-primary" style="float: right;" href="module/surat-jalan/proses/hapus-detail.php?xx=<?php echo $b['id']; ?>&master=<?php echo $master['id'] ?>">Ya Hapus</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $r++; $no++; } ?>
                                        </tbody>
                                    </table>

                                    <div id="txtHint"></div>
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <button type="submit" name="go_pesan" value="Pesan" class="btn btn-success" style="float:right;">
                                            Simpan
                                        </button>
                                        <a href="?page=report-surat-jalan" class="btn btn-danger" style="float:right; margin-right:10px;">
                                            Kembali
                                        </a>
                                    </div>
                            	</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function(){
            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
                echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
                unset($_SESSION['toastr']);
            }
            ?>          
        });


            $(document).on('click', '.remove', function() {
                var last = Number($("#lastRow").val());
                var ke   = $(this).attr('ke');
                if(ke == last){
                    var no   = Number($("#lastRow").val()) - Number(1);
                }else{
                    var no   = last;
                }
                $("#lastRow").val(no);
                $(this).parent().parent().remove();
            
            });

            $(document).ready(function () {
                $("#btn_add").click(function(){
                    var no = Number($("#lastRow").val()) + Number(1);

                    $("#body").append('<tr id="r['+no+']">'+
                        '<td>'+no+'</td>'+
                        '<input type="hidden" name="id_detail[]" id="id_detail[]" value="xxx">'+
                        '<td><input type="text" name="no_seri[]" id="no_seri[]" class="form-control"></td>'+
                        '<td><input type="text" name="nama_bahan[]" id="nama_bahan[]" class="form-control"></td>'+
                        '<td><input type="text" name="warna_bahan[]" id="warna_bahan[]" class="form-control"></td>'+
                        '<td><input type="text" name="jumlah[]" id="jumlah[]" class="form-control"></td>'+
                        '<td><input type="text" name="roll[]" id="roll[]" class="form-control"></td>'+
                        '<td><input type="text" name="kg[]" id="kg[]" class="form-control"></td>'+
                        '<td><input type="text" name="keterangan[]" id="keterangan[]" class="form-control"></td>'+
                        '<td><a class="btn btn-sm btn-danger remove" id="btn_remove" ke="'+no+'"><i class="fa fa-trash"></i></a></td>'+
                    '</tr>');      
                    $("#lastRow").val(no);

                });

            })

        </script>