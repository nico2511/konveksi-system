<?php
    include '../config/koneksi.php';
    if(isset($_GET['bulan']) && isset($_GET['tahun'])){
        $bulan_now = mysql_real_escape_string($_GET['bulan']);
        $tahun_now = mysql_real_escape_string($_GET['tahun']);
    }else{
        $bulan_now = date('m');
        $tahun_now = date('Y');
    }
    $url_ori   = str_replace('index.php','?page=produk-sk','http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Produk Keluar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Produk Keluar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td><span>Bulan</span></td>
                                    <td><span>Tahun</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="bulan" id="bulan" class="form-control">
                                            <option value="01" <?php if(01 == $bulan_now){ echo "selected"; } ?>>Januari</option>
                                            <option value="02" <?php if(02 == $bulan_now){ echo "selected"; } ?>>Februari</option>
                                            <option value="03" <?php if(03 == $bulan_now){ echo "selected"; } ?>>Maret</option>
                                            <option value="04" <?php if(04 == $bulan_now){ echo "selected"; } ?>>April</option>
                                            <option value="05" <?php if(05 == $bulan_now){ echo "selected"; } ?>>Mei</option>
                                            <option value="06" <?php if(06 == $bulan_now){ echo "selected"; } ?>>Juni</option>
                                            <option value="07" <?php if(07 == $bulan_now){ echo "selected"; } ?>>Juli</option>
                                            <option value="08" <?php if(8 == $bulan_now){ echo "selected"; } ?>>Agustus</option>
                                            <option value="09" <?php if(9 == $bulan_now){ echo "selected"; } ?>>September</option>
                                            <option value="10" <?php if(10 == $bulan_now){ echo "selected"; } ?>>Oktober</option>
                                            <option value="11" <?php if(11 == $bulan_now){ echo "selected"; } ?>>November</option>
                                            <option value="12" <?php if(12 == $bulan_now){ echo "selected"; } ?>>Desember</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="tahun" id="tahun" class="form-control" style="margin-left: 5px;">
                                            <?php for($t = date('Y')-3; $t <= date('Y') ; $t++){  ?>
                                                <option value="<?= $t ?>" <?php if($t == $tahun_now){ echo "selected"; } ?>><?php echo $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <button onclick="filter_x()" class="btn btn-primary" style="margin-left: 8px;">Filter</button>
                                    </td>
                                    <td>
                                        <a href="?page=produk-sk" class="btn btn-danger" style="margin-left: 3px;">Reset</a>
                                    </td>
                                </tr>
                            </table>
                            <input type="hidden" name="url_ori" id="url_ori" value="<?= $url_ori ?>">
                            <table id="example2" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Stok Awal</th>
                                        <th>Tujuan</th>
                                        <th>Keluar</th>
                                        <th>Stok Akhir</th>
                                        <th>Keterangan</th>
                                        <th>Periode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(isset($_GET['bulan']) && isset($_GET['tahun'])){
                                    $bln = mysql_real_escape_string($_GET['bulan']);
                                    $thn = mysql_real_escape_string($_GET['tahun']);
                                    $brg=mysql_query("SELECT * FROM keluar WHERE month(tgl) = '$bln' AND year(tgl) = '$thn' order by id desc");
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM keluar WHERE month(tgl) = '$now' AND year(tgl) = '$yea' order by id desc");
                                }
                                $no=1;
                                while($row=mysql_fetch_array($brg)){

                                    ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo date('d F Y',strtotime($row['tgl'])) ?></td>
                                        <td><?php echo $row['kode'] ?></td>
                                        <td><?php echo $row['nama'] ?></td>
                                        <td><?php echo $row['stok_awal'] ?></td>
                                        <td><?php echo $row['tujuan'] ?></td>
                                        <td><?php echo $row['jumlah'] ?></td>
                                        <td><?php echo $row['stok_akhir'] ?></td>
                                        <td><?php echo $row['keterangan'] ?></td>
                                        <td><?php echo $row['periode'] ?></td>
                                    </tr>

                                    <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function(){
            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
            echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
            unset($_SESSION['toastr']);
        }
        ?>          
    });

        function filter_x(){
            
            var bulan    = $('#bulan').val();
            var tahun    = $('#tahun').val();
            var url      = $('#url_ori').val();
            window.location = url+'&bulan='+bulan+'&tahun='+tahun;
        }
        
        function validasi() {
            // var kode_bb = document.getElementById('kode_bb');
            // var nama_bb = document.getElementById('nama_bb');
            // var harga   = document.getElementById('harga');
            // var warna   = document.getElementById('warna');
            // if(kode_bb.value==""){
            //     alert("maaf, kolom kode bahan baku harus diisi");
            //     kode_bb.focus();
            //     return false;
            // }else if(nama_bb.value==""){
            //     alert("maaf, kolom nama bahan baku harus diisi");
            //     nama_bb.focus();
            //     return false;
            // }else if(warna.value==""){
            //     alert("maaf, warna bahan baku harus diisi");
            //     warna.focus();
            //     return false;
            // }else if(harga.value==""){
            //     alert("maaf, kolom harga bahan baku harus diisi");
            //     harga.focus();
            //     return false;
            // }else{
            //     return true;
            // }
        }


        function valHarga() {
            var harga = document.getElementById('harga');
            if (isNaN(harga.value)) {
                alert("Maaf, kolom harga harus diisi angka");
                harga.focus();
                harga.value="";
            }
        }
    </script>