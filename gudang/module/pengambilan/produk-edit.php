<?php
    include '../config/koneksi.php';
    $id   = mysql_real_escape_string($_GET['id']);
    $data = mysql_fetch_array(mysql_query("SELECT * from produk where id = '$id'"));
    
?>

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Stok Masuk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form role="form-horizontal" action="module/bahan-jadi/proses/edit-produk.php" method="POST" onsubmit="return validasi_kirim()">
                                <div class="form-group">   
                                    <label>Jenis Produk</label>
                                    <select class="form-control" name="jenis">
                                        <?php 
                                        $brg=mysql_query("select * from jenis_produk");
                                        while($b=mysql_fetch_array($brg)){
                                            ?>	
                                            <option value="<?php echo $b['nama']; ?>" <?php if($b['nama'] == $data['jenis']){ echo "selected"; } ?>><?php echo $b['nama'] ?></option>
                                            <?php 
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" value="<?= $data['nama'] ?>" id="nama"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Kode</label>
                                    <input type="text" name="kode" value="<?= $data['kode'] ?>" id="kode"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Modal</label>
                                    <input type="text" name="modal" value="<?= $data['modal'] ?>" id="modal"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Harga Jual</label>
                                    <input type="text" name="harga" value="<?= $data['harga'] ?>" id="harga"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Stok</label>
                                    <input type="text" name="stok" value="<?= $data['stok'] ?>" id="stok"  class="form-control" readonly />
                                </div>
                                <div class="form-group">
                                    <label>Lokasi</label>
                                    <input type="hidden" name="id" id="id" value="<?= $data['id'] ?>">
                                    <input type="text" name="lokasi" id="lokasi" class="form-control" value="<?= $data['lokasi'] ?>" />
                                </div>
                                <div class="form-group">
                                    <label>Gambar</label>
                                    <input type="text" name="gambar" id="gambar" value="<?= $data['gambar'] ?>" class="form-control" />
                                </div>
                                <div id="txtHint"></div>
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" name="go_pesan" value="Pesan" class="btn btn-success" style="float:right;">
                                        Submit
                                    </button>
                                    <a href="?page=produk" class="btn btn-danger" style="float:right; margin-right:10px;">Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript">

        function validasi_kirim() {
            var stok    = document.getElementById('stok').value;
            var kg      = document.getElementById('kg').value;
            
            var periode = document.getElementById('periode').value;
            if(kg == ""){
                toastr.error('Kg tidak boleh kosong');
                return false;
            }else if(kg == 0){
                toastr.error('Kg tidak boleh kosong');
                return false;
            }else if(periode.value==""){
                toastr.error('periode tidak boleh kosong');
                return false;
            }else{
                return true;
            }
        }

        function cek_stok(){
            var stok    = document.getElementById('stok').value;
            var kg      = document.getElementById('jumlah_masuk').value;
            
            if(kg > stok){
                toastr.error('Stok tidak cukup');
                $("#jumlah_masuk").val(0);
            }
        }

            function valJml() {
                var jml = document.getElementById('jml');
                var harga = document.getElementById('harga');
                var total = document.getElementById('total');

                if (isNaN(jml.value)) {
                    alert("Maaf, kolom jumlah harus diisi angka");
                    jml.focus();
                    jml.value="";
                }else if(isNaN(harga.value)){
                    alert("Maaf, kolom harga harus diisi angka");
                    harga.focus();
                    harga.value="";
                }

                if(parseFloat(jml.value)){
                    total.value = jml.value * harga.value;
                }else if(parseFloat(harga.value)){
                    total.value = jml.value * harga.value;
                }else{
                    total.value="";
                }
            }
        </script>