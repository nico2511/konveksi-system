<?php
    include '../config/koneksi.php';
    if(isset($_GET['bulan']) && isset($_GET['tahun'])){
        $bulan_now = mysql_real_escape_string($_GET['bulan']);
        $tahun_now = mysql_real_escape_string($_GET['tahun']);
    }else{
        $bulan_now = date('m');
        $tahun_now = date('Y');
    }
    $url_ori   = str_replace('index.php','?page=pengambilan-umum','http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Pengambilan Umum</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Pengambilan Produk Jadi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td><span>Bulan</span></td>
                                    <td><span>Tahun</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="bulan" id="bulan" class="form-control">
                                            <option value="01" <?php if(01 == $bulan_now){ echo "selected"; } ?>>Januari</option>
                                            <option value="02" <?php if(02 == $bulan_now){ echo "selected"; } ?>>Februari</option>
                                            <option value="03" <?php if(03 == $bulan_now){ echo "selected"; } ?>>Maret</option>
                                            <option value="04" <?php if(04 == $bulan_now){ echo "selected"; } ?>>April</option>
                                            <option value="05" <?php if(05 == $bulan_now){ echo "selected"; } ?>>Mei</option>
                                            <option value="06" <?php if(06 == $bulan_now){ echo "selected"; } ?>>Juni</option>
                                            <option value="07" <?php if(07 == $bulan_now){ echo "selected"; } ?>>Juli</option>
                                            <option value="08" <?php if(8 == $bulan_now){ echo "selected"; } ?>>Agustus</option>
                                            <option value="09" <?php if(9 == $bulan_now){ echo "selected"; } ?>>September</option>
                                            <option value="10" <?php if(10 == $bulan_now){ echo "selected"; } ?>>Oktober</option>
                                            <option value="11" <?php if(11 == $bulan_now){ echo "selected"; } ?>>November</option>
                                            <option value="12" <?php if(12 == $bulan_now){ echo "selected"; } ?>>Desember</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="tahun" id="tahun" class="form-control" style="margin-left: 5px;">
                                            <?php for($t = date('Y')-3; $t <= date('Y') ; $t++){  ?>
                                                <option value="<?= $t ?>" <?php if($t == $tahun_now){ echo "selected"; } ?>><?php echo $t ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <button onclick="filter_x()" class="btn btn-primary" style="margin-left: 8px;">Filter</button>
                                    </td>
                                    <td>
                                        <a href="?page=pengambilan-umum" class="btn btn-danger" style="margin-left: 3px;">Reset</a>
                                    </td>
                                    <!-- <td>
                                        <a href="#modal-tambah" data-toggle="modal" class="btn btn-success" style="margin: left 3px;">Input Pengambilan Barang</a>
                                    </td> -->
                                </tr>
                            </table>
                            <input type="hidden" name="url_ori" id="url_ori" value="<?= $url_ori ?>">
                            <br>
                            <table id="example1" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>No Pengambilan</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Input By</th>
                                        <th>Periode</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(isset($_GET['bulan']) && isset($_GET['tahun'])){
                                    $bln = mysql_real_escape_string($_GET['bulan']);
                                    $thn = mysql_real_escape_string($_GET['tahun']);
                                    $brg=mysql_query("SELECT * FROM online WHERE month(tgl) = '$bln' AND year(tgl) = '$thn' AND status like 'Belum Dipotong Stok' AND type = 'Umum' order by id desc");
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM online WHERE month(tgl) = '$now' AND year(tgl) = '$yea' AND status like 'Belum Dipotong Stok' AND type = 'Umum' order by id desc");
                                }
                                $no=1;
                                while($b=mysql_fetch_array($brg)){

                                    ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo date('d F Y',strtotime($b['tgl'])) ?></td>
                                    <td><?php echo $b['no_pengambilan'] ?></td>
                                    <td><?php echo $b['nama'] ?></td>
                                    <td><?php echo str_replace('.-.pcs',' Pcs',$b['qty']) ?></td>
                                    <td><?php echo $b['created_by'] ?></td>
                                    <td><?php echo $b['periode'] ?></td>
                                    <td><?php echo $b['status'] ?></td>
                                    <td><a href="#modal-hapus<?= $b['id'] ?>" data-toggle="modal" class="btn btn-info">Potong Stok</a> &nbsp;</td>
                                </tr>

                                    <div class="modal fade bs-example-modal-md" id="modal-hapus<?= $b['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                        <div class="modal-dialog modal-md" style="margin-top:50px;" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi Potong Stok</h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                </div>
                                                <div class="modal-body" >
                                                    Apakah kamu yakin ingin potong stok <b><?= $b['nama'] ?> - <?= $b['qty'] ?> Pcs ?</b>
                                                    <form role="form-horizontal" action="module/pengambilan/proses/potong-stok-umum.php" method="POST">
                                                            <input type="hidden" name="id" readonly value="<?= $b['id'] ?>" id="id"  class="form-control" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                    <button type="submit" name="go" value="hapus" class="btn btn-primary" style="float:right;">Ya, Potong</button>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal -->
    <div id="modal-tambah" class="modal fade">
	    <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
                    <h4 class="modal-title">Input Pengambilan Barang Da Yopi</h4>
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">				
                    <form role="form-horizontal" action="module/pengambilan/proses/input-pengambilan-dayopi.php" method="POST" onsubmit="return validasi()">
						<div class="form-group">
							<label>Tanggal</label>
							<input name="tgl" type="date" class="form-control" id="tgl" value="<?= date('Y-m-d') ?>" required>
						</div>
						<div class="form-group">
							<label>Nama Barang</label>								
							<select class="form-control select2" id="nama" name="nama" onchange="ambil_stok()" required>
                                <option value="-">- Pilih Produk -</option>
								<?php 
								$brg=mysql_query("select * from produk");
								while($b=mysql_fetch_array($brg)){
									?>	
									<option value="<?php echo $b['id'].'?'.$b['stok']; ?>"><?php echo $b['nama'] ?> Stok : <?php echo $b['stok'] ?></option>
									<?php 
								}
								?>
							</select>
						</div>
                        <input type="hidden" name="stok_now" id="stok_now">
						<div class="form-group">
							<label>Jumlah</label>
							<input name="qty" id="jumlah" type="text" class="form-control" onkeyup="cek_stok()" placeholder="Jumlah" autocomplete="off" required>
						</div>
						<div class="form-group">
							<label>Periode</label>								
							<select class="form-control" name="periode">
								<?php 
								$brg=mysql_query("select * from periode order by id desc");
								while($b=mysql_fetch_array($brg)){
									?>	
									<option value="<?php echo $b['bulan']; ?>"><?php echo $b['bulan'] ?></option>
									<?php 
								}
								?>
							</select>
						</div>
                </div>
                <div class="modal-footer">												
                    <input type="submit" class="btn btn-primary" value="Simpan">
                </div>
				    </form>
			</div>
		</div>
	</div>	
    <!-- end modal -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!-- select2  -->
    
    <script type="text/javascript">

        $(function(){

            
            <?php
                // toastr output & session reset
                
            session_start();
            if(isset($_SESSION['toastr'])){
                echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
                unset($_SESSION['toastr']);
            }
            ?>          
        });

        function filter_x(){
            
            var bulan    = $('#bulan').val();
            var tahun    = $('#tahun').val();
            var url      = $('#url_ori').val();
            window.location = url+'&bulan='+bulan+'&tahun='+tahun;
        }

        function validasi(){
            var konsumen    = $('#konsumen').val();
            var nama        = $('#nama').val();
            
            if(nama == '-'){
                toastr.error('Silahkan pilih produk');
                return false;
            }else if(konsumen == '-'){
                toastr.error('Silahkan pilih konsumen');
                return false;
            }else{
                return true;
            }
        }
        
        function ambil_stok(){
            var x      = document.getElementById('nama').value;
            const stok = x.split("?");
            $("#stok_now").val(stok[1]);
            // var keluar  = document.getElementById('keluar').value;
            // if(Number(keluar) > Number(stok)){
            //     toastr.error('Stok tidak cukup');
            // $("#keluar").val(0);
            // }
        }
        function cek_stok(){
            var stok      = document.getElementById('stok_now').value;
            var keluar   = document.getElementById('jumlah').value;
            if(Number(keluar) > Number(stok)){
                toastr.error('Stok tidak cukup');
                $("#jumlah").val(stok);
            }
        }
    </script>