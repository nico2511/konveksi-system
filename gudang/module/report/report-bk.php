<?php
    include '../config/koneksi.php';
    if(isset($_GET['produk'])){
        $produk_now = mysql_real_escape_string($_GET['produk']);
    }
    
    $range = $_GET['date_range'];
    
    $url_ori   = str_replace('index.php','?page=report-bk','http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Bahan Keluar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Laporan Bahan Keluar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td><span>Bahan</span></td>
                                    <td><span class="ml-2">Tanggal</span></td>
                                    <!-- <td><span class="ml-2">Status</span></td> -->
                                </tr>
                                <tr>
                                    <td>
                                        <select name="produk" id="produk" class="form-control select2">
                                            <option value="semua">Semua</option>
                                        <?php 
                                            $konsumen=mysql_query("SELECT * from bahan_baku ORDER BY nama_bahan_baku ASC");
                                            while($b=mysql_fetch_array($konsumen)){
                                                ?>	
                                                <option value="<?php echo $b['kode_bahan_baku']; ?>" <?php if($b['kode_bahan_baku'] == $produk_now){ echo "selected"; } ?>><?php echo $b['nama_bahan_baku'].' - '.$b['warna']; ?></option>
                                                <?php 
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group ml-2">
                                            <button type="button" class="btn btn-default float-right" name="btn_range" id="range-btn">
                                            <i class="far fa-calendar-alt"></i> <span id="mask_range"> Bulan ini</span>
                                            <i class="fas fa-caret-down"></i>
                                            </button>
                                        </div>
                                        <input type="hidden" name="date_range" id="date_range">
                                    </td>
                                    <!-- <td>
                                        <select name="status" id="status" class="form-control ml-2">
                                                <option value="semua">Semua</option>
                                                <option value="1" <?php if($status == '1'){ echo "selected"; } ?>>Sudah dipotong stok</option>
                                                <option value="0" <?php if($status == '0'){ echo "selected"; } ?>>Belum dipotong stok</option>
                                        </select>
                                    </td> -->
                                    <td>
                                        <button onclick="filter_x()" class="btn btn-primary ml-4">Filter</button>
                                    </td>
                                    <td>
                                        <a href="?page=report-bk" class="btn btn-danger" style="margin-left: 3px;">Reset</a>
                                    </td>
                                </tr>
                                <br>
                            </table>
                            <br>
                            <?php if(isset($_GET['produk'])){ ?>
                                <a href="module/report/cetak-excel-bk.php?produk=<?= $_GET['produk'] ?>&date_range=<?= $_GET['date_range'] ?>" target="_blank" class="btn btn-success" style="margin-left: 3px;">Export Excel</a>  
                                <a href="module/report/cetak-pdf-bk.php?produk=<?= $_GET['produk'] ?>&date_range=<?= $_GET['date_range'] ?>" target="_blank" class="btn btn-danger" style="margin-left: 3px;">Export PDF</a>  
                            <?php }else{ ?>
                                <a href="module/report/cetak-excel-bk.php" target="_blank" class="btn btn-success" style="margin-left: 3px;">Export Excel</a>  
                                <a href="module/report/cetak-pdf-bk.php" target="_blank" class="btn btn-danger" style="margin-left: 3px;">Export PDF</a>  
                                <?php } ?>
                            <input type="hidden" name="url_ori" id="url_ori" value="<?= $url_ori ?>">
                            <br>
                            <br>
                            <table id="example2" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Nama Bahan</th>
                                        <th>Warna</th>
                                        <th>Roll Awal</th>
                                        <th>Kg Awal</th>
                                        <th>Roll</th>
                                        <th>Kg</th>
                                        <th>Roll Akhir</th>
                                        <th>Kg Akhir</th>
                                        <th>Tujuan</th>
                                        <th>Keterangan</th>
                                        <th>Periode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(isset($_GET['produk']) && isset($_GET['date_range'])){
                                    $quer = 'SELECT * FROM bahan_keluar';
                                    $p = mysql_real_escape_string($_GET['produk']);
                                    if($p != 'semua'){
                                        $quer = $quer." WHERE nama = '$p'";
                                    }

                                    $d      = mysql_real_escape_string($_GET['date_range']);
                                    $pecah  = explode('X',$range);
                                    $from   = $pecah[0];
                                    $to     = $pecah[1];
                                    $now    = date('m');
                                    $yea    = date('Y');
                                    if($p == 'semua'){
                                        if($d != ""){
                                            $quer = $quer." WHERE tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." WHERE month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }else{
                                        if($d != ""){
                                            $quer = $quer." AND tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." AND month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }

                                    $brg=mysql_query($quer.' ORDER BY id DESC');
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM bahan_keluar WHERE month(tgl) = '$now' AND year(tgl) = '$yea' order by id desc");
                                }
                                $no=1;
                                while($row=mysql_fetch_array($brg)){
                                    $kode = $row['nama'];
                                    $prod = mysql_fetch_array(mysql_query("SELECT * FROM bahan_baku WHERE kode_bahan_baku = '$kode'"));
                                    ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo date('d F Y',strtotime($row['tgl'])) ?></td> 
                                        <td><?php echo $prod['nama_bahan_baku'] ?></td>
                                        <td><?php echo $prod['warna'] ?></td>
                                        <td><?php echo $row['roll_awal'] ?></td>
                                        <td><?php echo $row['kg_awal'] ?></td>
                                        <td><?php echo $row['roll'] ?></td>
                                        <td><?php echo $row['kg'] ?></td>
                                        <td><?php echo $row['roll_akhir'] ?></td>
                                        <td><?php echo $row['kg_akhir'] ?></td>                                        
                                        <td><?php echo $row['tujuan'] ?></td>
                                        <td><?php echo $row['keterangan'] ?></td>
                                        <td><?php echo $row['periode'] ?></td>
                                    </tr>

                                    <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!-- select2  -->
    
    <script type="text/javascript">

        $(function(){

            
            <?php
                // toastr output & session reset

            if(isset($_GET['date_range'])){ 
                $pc = explode('X',$_GET['date_range']);
                ?>
                $('#date_range').val('<?php echo date('Y-m-d',strtotime($pc[0])).'X'.date('Y-m-d',strtotime($pc[1])) ?>');
                $('#mask_range').html('<?php echo date('d F Y',strtotime($pc[0])).' - '.date('d F Y',strtotime($pc[1])) ?>');
            <?php }
                
            session_start();
            if(isset($_SESSION['toastr'])){
                echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
                unset($_SESSION['toastr']);
            }
            ?>        
            
            $('#range-btn').daterangepicker(
                {
                    ranges   : {
                    'Hari ini'       : [moment(), moment()],
                    'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari yang lalu' : [moment().subtract(6, 'days'), moment()],
                    '30 Hari yang lalu': [moment().subtract(29, 'days'), moment()],
                    'Bulan ini'  : [moment().startOf('month'), moment().endOf('month')],
                    'Bulan kemarin'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#mask_range').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'))
                    $('#date_range').val(start.format('YYYY-MM-DD') + 'X' + end.format('YYYY-MM-DD'))
                }
            );
        });

        function filter_x(){
            
            var produk      = $('#produk').val();        
            var date_range  = $('#date_range').val();
            var url         = $('#url_ori').val();
            window.location = url+'&produk='+produk+'&date_range='+date_range;
        }

        function validasi(){
            var konsumen    = $('#konsumen').val();
            var nama        = $('#nama').val();
            var jumlah        = $('#jumlah').val();
            
            if(nama == '-'){
                toastr.error('Silahkan pilih produk');
                return false;
            }else if(konsumen == '-'){
                toastr.error('Silahkan pilih konsumen');
                return false;
            }else if(jumlah == 0 || jumlah == ''){
                toastr.error('Minimal pengeluaran 1');
                return false;
            }else{
                return true;
            }
        }
        
    </script>