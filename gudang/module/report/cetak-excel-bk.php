<?php 
    include '../../../config/koneksi.php';
    session_start();
  
    if(!isset($_SESSION['username'])){
      header("location:../../../login/");
    }
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Export Data Excel - PT.AFIT</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;
 
	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>
 
	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Laporan Bahan Keluar.xls");
	?>
 
	<center>
		<h3>Laporan Bahan Keluar</h3>
	</center>
 
	<table border="1">
    <tr style="background-color: yellow;">
    <th class="text-center">No</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Warna</th>
                                        <th class="text-center">Roll Awal</th>
                                        <th class="text-center">Kg Awal</th>
                                        <th class="text-center">Roll</th>
                                        <th class="text-center">Kg</th>
                                        <th class="text-center">Roll Akhir</th>
                                        <th class="text-center">Kg Akhir</th>
                                        <th class="text-center">Tujuan</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Periode</th>
                                    </tr>
                                    <?php 
                                if(isset($_GET['produk']) && isset($_GET['date_range'])){
                                    $quer = 'SELECT * FROM bahan_keluar';
                                    $p = mysql_real_escape_string($_GET['produk']);
                                    if($p != 'semua'){
                                        $quer = $quer." WHERE nama = '$p'";
                                    }

                                    $d      = mysql_real_escape_string($_GET['date_range']);
                                    $pecah  = explode('X',$d);
                                    $from   = $pecah[0];
                                    $to     = $pecah[1];
                                    $now    = date('m');
                                    $yea    = date('Y');
                                    if($p == 'semua'){
                                        if($d != ""){
                                            $quer = $quer." WHERE tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." WHERE month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }else{
                                        if($d != ""){
                                            $quer = $quer." AND tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." AND month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }

                                    $brg=mysql_query($quer.' ORDER BY id DESC');
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM bahan_keluar WHERE month(tgl) = '$now' AND year(tgl) = '$yea' order by id desc");
                                }
                                $no=1;
                                while($b=mysql_fetch_array($brg)){
                                    $kode = $b['nama'];
                                    $sp   = $b['suplier'];
                                    $spl  = mysql_fetch_array(mysql_query("SELECT * FROM supplier WHERE kode_supplier = '$sp'"));
                                    $prod = mysql_fetch_array(mysql_query("SELECT * FROM bahan_baku WHERE kode_bahan_baku = '$kode'"));
                                    ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo date('d F Y',strtotime($b['tgl'])) ?></td>
                                    <td><?php echo $prod['nama_bahan_baku'] ?></td>
                                    <td><?php echo $prod['warna'] ?></td>
                                    <td><?php echo $b['roll_awal'] ?></td>
                                    <td><?php echo $b['kg_awal'] ?></td>
                                    <td><?php echo $b['roll'] ?></td>
                                    <td><?php echo $b['kg'] ?></td>
                                    <td><?php echo $b['roll_akhir'] ?></td>
                                    <td><?php echo $b['kg_akhir'] ?></td>
                                    <td><?php echo $b['tujuan'] ?></td>
                                    <td><?php echo $b['keterangan'] ?></td>
                                    <td><?php echo $b['periode'] ?></td>
                                </tr>

                                    <?php 
                                }
                                ?>
	</table>
</body>
</html>