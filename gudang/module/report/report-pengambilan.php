<?php
    include '../config/koneksi.php';
    if(isset($_GET['konsumen'])){
        $konsumen_now = mysql_real_escape_string($_GET['konsumen']);
        $status       = mysql_real_escape_string($_GET['status']);
    }
    
    $range = $_GET['date_range'];
    $url_ori   = str_replace('index.php','?page=report-pengambilan','http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Pengambilan Barang</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Laporan Pengambilan Barang</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table>
                                <tr>
                                    <td><span>Konsumen</span></td>
                                    <td><span class="ml-2">Tanggal</span></td>
                                    <td><span class="ml-2">Status</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="konsumen" id="konsumen" class="form-control select2">
                                            <option value="semua">Semua</option>
                                        <?php 
                                            $konsumen=mysql_query("SELECT * from konsumen ORDER BY nama ASC");
                                            while($b=mysql_fetch_array($konsumen)){
                                                ?>	
                                                <option value="<?php echo $b['nama']; ?>" <?php if($b['nama'] == $konsumen_now){ echo "selected"; } ?>><?php echo $b['nama']; ?></option>
                                                <?php 
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group ml-2">
                                            <button type="button" class="btn btn-default float-right" name="btn_range" id="range-btn">
                                            <i class="far fa-calendar-alt"></i> <span id="mask_range"> Bulan ini</span>
                                            <i class="fas fa-caret-down"></i>
                                            </button>
                                        </div>
                                        <input type="hidden" name="date_range" id="date_range">
                                    </td>
                                    <td>
                                        <select name="status" id="status" class="form-control ml-2">
                                                <option value="semua">Semua</option>
                                                <option value="1" <?php if($status == '1'){ echo "selected"; } ?>>Sudah dipotong stok</option>
                                                <option value="0" <?php if($status == '0'){ echo "selected"; } ?>>Belum dipotong stok</option>
                                        </select>
                                    </td>
                                    <td>
                                        <button onclick="filter_x()" class="btn btn-primary ml-4">Filter</button>
                                    </td>
                                    <td>
                                        <a href="?page=report-pengambilan" class="btn btn-danger" style="margin-left: 3px;">Reset</a>
                                    </td>
                                </tr>
                                <br>
                            </table>
                            <br>
                            <?php if(isset($_GET['konsumen'])){ ?>
                                <a href="module/report/cetak-excel-pengambilan.php?konsumen=<?= $_GET['konsumen'] ?>&date_range=<?= $_GET['date_range'] ?>&status=<?= $_GET['status'] ?>" target="_blank" class="btn btn-success" style="margin-left: 3px;">Export Excel</a>  
                                <a href="module/report/cetak-pdf-pengambilan.php?konsumen=<?= $_GET['konsumen'] ?>&date_range=<?= $_GET['date_range'] ?>&status=<?= $_GET['status'] ?>" target="_blank" class="btn btn-danger" style="margin-left: 3px;">Export PDF</a>  
                            <?php }else{ ?>
                                <a href="module/report/cetak-excel-pengambilan.php" target="_blank" class="btn btn-success" style="margin-left: 3px;">Export Excel</a>  
                                <a href="module/report/cetak-pdf-pengambilan.php" target="_blank" class="btn btn-danger" style="margin-left: 3px;">Export PDF</a>  
                                <?php } ?>
                            <input type="hidden" name="url_ori" id="url_ori" value="<?= $url_ori ?>">
                            <br>
                            <br>
                            <table id="example2" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Konsumen</th>
                                        <th>Nama Barang</th>
                                        <th>Warna</th>
                                        <th>Stok Awal</th>
                                        <th>Jumlah</th>
                                        <th>Stok Akhir</th>
                                        <th>Periode</th>
                                        <th>Input By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(isset($_GET['konsumen']) && isset($_GET['date_range']) && isset($_GET['status'])){
                                    $quer = 'SELECT * FROM online';
                                    $k = mysql_real_escape_string($_GET['konsumen']);
                                    if($k != 'semua'){
                                        $quer = $quer." WHERE konsumen = '$k'";
                                    }

                                    $d = mysql_real_escape_string($_GET['date_range']);

                                    $s = mysql_real_escape_string($_GET['status']);
                                    if($s == '0'){
                                        if($k == 'semua'){
                                            $quer = $quer." WHERE status = 'Belum Dipotong Stok'";
                                        }else{
                                            $quer = $quer." AND status = 'Belum Dipotong Stok'";
                                        }
                                    }else if($s == '1'){
                                        if($k == 'semua'){
                                            $quer = $quer." WHERE status = 'sudah dipotong stok'";
                                        }else{
                                            $quer = $quer." AND status = 'sudah dipotong stok'";
                                        }
                                    }

                                    $brg=mysql_query($quer.' ORDER BY id DESC');
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM online WHERE month(tgl) = '$now' AND year(tgl) = '$yea' order by id desc");
                                }
                                $no=1;
                                while($b=mysql_fetch_array($brg)){
                                    $peng = $b['no_pengambilan'];
                                    $pr_id= $b['produk_id'];
                                    $cek  = mysql_fetch_array(mysql_query("SELECT * FROM keluar WHERE no_pengambilan = '$peng'"));
                                    $prod = mysql_fetch_array(mysql_query("SELECT * FROM produk WHERE id = '$pr_id'"));
                                    ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo date('d F Y',strtotime($b['tgl'])) ?></td>
                                    <td><?php echo $b['konsumen'] ?></td>
                                    <td><?php echo $b['nama'] ?></td>
                                    <td><?php echo $prod['warna'] ?></td>
                                    <td><?php echo $cek['stok_awal'] ?></td>
                                    <td><?php echo $b['qty'] ?></td>
                                    <td><?php echo $cek['stok_akhir'] ?></td>
                                    <td><?php echo $b['periode'] ?></td>
                                    <td><?php echo $b['created_by'] ?></td>
                                </tr>

                                    <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <!-- select2  -->
    
    <script type="text/javascript">

        $(function(){

            
            <?php
                // toastr output & session reset

            if(isset($_GET['date_range'])){ 
                $pc = explode('X',$_GET['date_range']);
                ?>
                $('#date_range').val('<?php echo date('Y-m-d',strtotime($pc[0])).'X'.date('Y-m-d',strtotime($pc[1])) ?>');
                $('#mask_range').html('<?php echo date('d F Y',strtotime($pc[0])).' - '.date('d F Y',strtotime($pc[1])) ?>');
            <?php }
                
            session_start();
            if(isset($_SESSION['toastr'])){
                echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
                unset($_SESSION['toastr']);
            }
            ?>        
            
            $('#range-btn').daterangepicker(
                {
                    ranges   : {
                    'Hari ini'       : [moment(), moment()],
                    'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari yang lalu' : [moment().subtract(6, 'days'), moment()],
                    '30 Hari yang lalu': [moment().subtract(29, 'days'), moment()],
                    'Bulan ini'  : [moment().startOf('month'), moment().endOf('month')],
                    'Bulan kemarin'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#mask_range').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'))
                    $('#date_range').val(start.format('YYYY-MM-DD') + 'X' + end.format('YYYY-MM-DD'))
                }
            );
        });

        function filter_x(){
            
            var konsumen    = $('#konsumen').val();
            var status      = $('#status').val();
            var date_range  = $('#date_range').val();
            var url         = $('#url_ori').val();
            window.location = url+'&konsumen='+konsumen+'&date_range='+date_range+'&status='+status;
        }

        function validasi(){
            var konsumen    = $('#konsumen').val();
            var nama        = $('#nama').val();
            var jumlah        = $('#jumlah').val();
            
            if(nama == '-'){
                toastr.error('Silahkan pilih produk');
                return false;
            }else if(konsumen == '-'){
                toastr.error('Silahkan pilih konsumen');
                return false;
            }else if(jumlah == 0 || jumlah == ''){
                toastr.error('Minimal pengeluaran 1');
                return false;
            }else{
                return true;
            }
        }
        
        function ambil_stok(){
            var x      = document.getElementById('nama').value;
            const stok = x.split("?");
            $("#stok_now").val(stok[1]);
            // var keluar  = document.getElementById('keluar').value;
            // if(Number(keluar) > Number(stok)){
            //     toastr.error('Stok tidak cukup');
            // $("#keluar").val(0);
            // }
        }
        function cek_stok(){
            var stok      = document.getElementById('stok_now').value;
            var keluar   = document.getElementById('jumlah').value;
            if(Number(keluar) > Number(stok)){
                toastr.error('Stok tidak cukup');
                $("#jumlah").val(stok);
            }
        }
    </script>