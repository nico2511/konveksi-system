<?php
  include '../../../config/koneksi.php';
  session_start();

  if(!isset($_SESSION['username'])){
    header("location:../../../login/");
  }
?>    

<html>
    <head>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <style>
            #invoice{
                padding: 30px;
            }

            .invoice {
                position: relative;
                background-color: #FFF;
                min-height: 680px;
                padding: 15px
            }

            .invoice header {
                padding: 10px 0;
                margin-bottom: 20px;
                border-bottom: 1px solid #3989c6
            }

            .invoice .company-details {
                text-align: right
            }

            .invoice .company-details .name {
                margin-top: 0;
                margin-bottom: 0
            }

            .invoice .contacts {
                margin-bottom: 20px
            }

            .invoice .invoice-to {
                text-align: left
            }

            .invoice .invoice-to .to {
                margin-top: 0;
                margin-bottom: 0
            }

            .invoice .invoice-details {
                text-align: right
            }

            .invoice .invoice-details .invoice-id {
                margin-top: 0;
                color: #3989c6;
                font-size: 32px;
            }

            .invoice main {
                padding-bottom: 50px
            }

            .invoice main .thanks {
                margin-top: -100px;
                font-size: 2em;
                margin-bottom: 50px
            }

            .invoice main .notices {
                padding-left: 6px;
                border-left: 6px solid #3989c6
            }

            .invoice main .notices .notice {
                font-size: 1.2em
            }


            .invoice footer {
                width: 100%;
                text-align: center;
                color: #777;
                border-top: 1px solid #aaa;
                padding: 8px 0
            }

            @media print {
                html, body {
                    border: 1px solid white;
                    height: 99%;
                    page-break-after: avoid;
                    page-break-before: avoid;
                }
            }

            /* @media print {
                .invoice {
                    font-size: 11px!important;
                    overflow: hidden!important
                }

                .invoice footer {
                    position: absolute;
                    bottom: 10px;
                    page-break-after: always
                }

                .invoice>div:last-child {
                    page-break-before: always
                }
            } */
        </style>
    </head>

    <body onload="window.print()">
        <div id="invoice">

            <div class="invoice overflow-auto">
                <div style="min-width: 600px">
                    <header>
                        <div class="row">
                            <div class="col">
                                <img src="../../../login/gambar/afit.png" data-holder-rendered="true" />
                            </div>
                            <div class="col company-details">
                                <h3 class="name">
                                    PT.Andria Fesyen Indonesia Tekstil
                                </h3><br>
                                <h5 style="margin-top: -13px;"><i>The Right Choice For Business Partner</i></h5>
                                <div><small>Jl.Panda V No. 197 Pondok Ranji, Ciputat Timur, Tangerang Selatan 15412</small></div>
                                <div><small>(+62) 812-9900-9777</small></div>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row contacts">
                            <div class="col invoice-to">
                                <h4 class="to"><?= $master['kode'] ?></h4>
                            </div>
                            <div class="col invoice-details">
                                <h1 class="invoice-id">Laporan Bahan Keluar</h1>
                                <div class="date">Dicetak Pada : <?= date('d F Y') ?> </div>
                            </div>
                        </div>
                        <table border="1" cellspacing="2" cellpadding="2" width="100%">
                        <thead>
                        <tr style="background-color: yellow;">
                                        <th class="text-center">No</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Warna</th>
                                        <th class="text-center">Roll Awal</th>
                                        <th class="text-center">Kg Awal</th>
                                        <th class="text-center">Roll</th>
                                        <th class="text-center">Kg</th>
                                        <th class="text-center">Roll Akhir</th>
                                        <th class="text-center">Kg Akhir</th>
                                        <th class="text-center">Tujuan</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Periode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(isset($_GET['produk']) && isset($_GET['date_range'])){
                                    $quer = 'SELECT * FROM bahan_keluar';
                                    $p = mysql_real_escape_string($_GET['produk']);
                                    if($p != 'semua'){
                                        $quer = $quer." WHERE nama = '$p'";
                                    }

                                    $d      = mysql_real_escape_string($_GET['date_range']);
                                    $pecah  = explode('X',$d);
                                    $from   = $pecah[0];
                                    $to     = $pecah[1];
                                    $now    = date('m');
                                    $yea    = date('Y');
                                    if($p == 'semua'){
                                        if($d != ""){
                                            $quer = $quer." WHERE tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." WHERE month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }else{
                                        if($d != ""){
                                            $quer = $quer." AND tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." AND month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }

                                    $brg=mysql_query($quer.' ORDER BY id DESC');
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM bahan_keluar WHERE month(tgl) = '$now' AND year(tgl) = '$yea' order by id desc");
                                }
                                $no=1;
                                while($b=mysql_fetch_array($brg)){
                                    $kode = $b['nama'];
                                    $sp   = $b['suplier'];
                                    $spl  = mysql_fetch_array(mysql_query("SELECT * FROM supplier WHERE kode_supplier = '$sp'"));
                                    $prod = mysql_fetch_array(mysql_query("SELECT * FROM bahan_baku WHERE kode_bahan_baku = '$kode'"));
                                    ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo date('d F Y',strtotime($b['tgl'])) ?></td>
                                    <td><?php echo $prod['nama_bahan_baku'] ?></td>
                                    <td><?php echo $prod['warna'] ?></td>
                                    <td><?php echo $b['roll_awal'] ?></td>
                                    <td><?php echo $b['kg_awal'] ?></td>
                                    <td><?php echo $b['roll'] ?></td>
                                    <td><?php echo $b['kg'] ?></td>
                                    <td><?php echo $b['roll_akhir'] ?></td>
                                    <td><?php echo $b['kg_akhir'] ?></td>
                                    <td><?php echo $b['tujuan'] ?></td>
                                    <td><?php echo $b['keterangan'] ?></td>
                                    <td><?php echo $b['periode'] ?></td>
                                </tr>

                                    <?php 
                                }
                                ?>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <br>
<!--                     
                        <div class="notices">
                            <table border ='0' align='left' cellpadding='0' cellspacing='0' >
                                <tr>
                                    <td align='center' style="font-size:15px;"><b>Dibuat Oleh</b></td>
                                    <td align='center' style="font-size:15px;"><b>Diketahui Oleh</b></td>
                                </tr>
                                <tr>
                                    <td align="center" width='500' height='150' style="font-size:13px;"><i></i></td>
                                    <td align="center" width='500' height='150' style="font-size:13px;">Puji Ati</td>
                                </tr>   
                            </table>
                        </div> -->
                    </main>
                <!-- <footer>
                    Invoice was created on a computer and is valid without the signature and seal.
                </footer> -->
                </div>
            <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
            <div>
                
        </div>
    </body>
    <script>
        var css = '@page { size: landscape; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    style.media = 'print';

    if (style.styleSheet){
    style.styleSheet.cssText = css;
    } else {
    style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
    </script>
</html>