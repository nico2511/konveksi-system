<?php 
    include '../../../config/koneksi.php';
    session_start();
  
    if(!isset($_SESSION['username'])){
      header("location:../../../login/");
    }
    $id = $_GET['id'];
    $master = mysql_fetch_array(mysql_query("SELECT * FROM sj_master WHERE id = '$id'"));
    $detail = mysql_query("SELECT * FROM sj_detail WHERE sj_id = '$id' ORDER BY id ASC");
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Export Data Ke Excel Dengan PHP - www.malasngoding.com</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;
 
	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>
 
	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Laporan Pengambilan Barang.xls");
	?>
 
	<center>
		<h3>Laporan Pengambilan Barang</h3>
	</center>
 
	<table border="1">
        <tr style="background-color: yellow;">
        <th class="text-center">No</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Konsumen</th>
                                        <th class="text-center">Nama Barang</th>
                                        <th class="text-center">Warna</th>
                                        <th class="text-center">Stok Awal</th>
                                        <th class="text-center">Jumlah</th>
                                        <th class="text-center">Stok Akhir</th>
                                        <th class="text-center">Periode</th>
                                        <th class="text-center">Input By</th>
        </tr>
		<?php 
                                if(isset($_GET['konsumen']) && isset($_GET['date_range']) && isset($_GET['status'])){
                                    $quer = 'SELECT * FROM online';
                                    $k = mysql_real_escape_string($_GET['konsumen']);
                                    if($k != 'semua'){
                                        $quer = $quer." WHERE konsumen = '$k'";
                                    }

                                    $d = mysql_real_escape_string($_GET['date_range']);

                                    $s = mysql_real_escape_string($_GET['status']);
                                    if($s == '0'){
                                        if($k == 'semua'){
                                            $quer = $quer." WHERE status = 'Belum Dipotong Stok'";
                                        }else{
                                            $quer = $quer." AND status = 'Belum Dipotong Stok'";
                                        }
                                    }else if($s == '1'){
                                        if($k == 'semua'){
                                            $quer = $quer." WHERE status = 'sudah dipotong stok'";
                                        }else{
                                            $quer = $quer." AND status = 'sudah dipotong stok'";
                                        }
                                    }

                                    $brg=mysql_query($quer.' ORDER BY id DESC');
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM online WHERE month(tgl) = '$now' AND year(tgl) = '$yea' order by id desc");
                                }
                                $no=1;
                                while($b=mysql_fetch_array($brg)){
                                    $peng = $b['no_pengambilan'];
                                    $pr_id= $b['produk_id'];
                                    $cek  = mysql_fetch_array(mysql_query("SELECT * FROM keluar WHERE no_pengambilan = '$peng'"));
                                    $prod = mysql_fetch_array(mysql_query("SELECT * FROM produk WHERE id = '$pr_id'"));
                                    ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo date('d F Y',strtotime($b['tgl'])) ?></td>
                                    <td><?php echo $b['konsumen'] ?></td>
                                    <td><?php echo $b['nama'] ?></td>
                                    <td><?php echo $prod['warna'] ?></td>
                                    <td><?php echo $cek['stok_awal'] ?></td>
                                    <td><?php echo $b['qty'] ?></td>
                                    <td><?php echo $cek['stok_akhir'] ?></td>
                                    <td><?php echo $b['periode'] ?></td>
                                    <td><?php echo $b['created_by'] ?></td>
                                </tr>

                                    <?php 
                                }
                                ?>
	</table>
</body>
</html>