<?php 
    include '../../../config/koneksi.php';
    session_start();
  
    if(!isset($_SESSION['username'])){
      header("location:../../../login/");
    }
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Export Data Excel - PT.AFIT</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;
 
	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>
 
	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=Laporan Stok Keluar.xls");
	?>
 
	<center>
		<h3>Laporan Stok Masuk</h3>
	</center>
 
	<table border="1">
    <tr style="background-color: yellow;">
                            <th class="text-center">No</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">Kode</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Warna</th>
                                        <th class="text-center">Stok Awal</th>
                                        <th class="text-center">Tujuan</th>
                                        <th class="text-center">Keluar</th>
                                        <th class="text-center">Stok Akhir</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Periode</th>
                                    </tr>
                                <?php 
                                if(isset($_GET['produk']) && isset($_GET['date_range'])){
                                    $quer = 'SELECT * FROM keluar';
                                    $p = mysql_real_escape_string($_GET['produk']);
                                    if($p != 'semua'){
                                        $quer = $quer." WHERE kode = '$p'";
                                    }

                                    $d      = mysql_real_escape_string($_GET['date_range']);
                                    $range  = $_GET['date_range'];
                                    $pecah  = explode('X',$range);
                                    $from   = $pecah[0];
                                    $to     = $pecah[1];
                                    $now    = date('m');
                                    $yea    = date('Y');
                                    if($p == 'semua'){
                                        if($d != ""){
                                            $quer = $quer." WHERE tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." WHERE month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }else{
                                        if($d != ""){
                                            $quer = $quer." AND tgl >= '$from' AND tgl   <= '$to'";
                                        }else{
                                            $quer = $quer." AND month(tgl) = '$now' AND year(tgl) = '$yea'";
                                        }
                                    }
                                    $brg=mysql_query($quer.' ORDER BY id DESC');
                                }else{
                                    $now = date('m');
                                    $yea = date('Y');
                                    $brg=mysql_query("SELECT * FROM keluar WHERE month(tgl) = '$now' AND year(tgl) = '$yea' order by id desc");
                                }
                                $no=1;
                                while($row=mysql_fetch_array($brg)){
                                    $kode = $row['kode'];
                                    $prod = mysql_fetch_array(mysql_query("SELECT * FROM produk WHERE kode = '$kode'"));
                                    ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo date('d F Y',strtotime($row['tgl'])) ?></td>
                                        <td><?php echo $row['kode'] ?></td>
                                        <td><?php echo $prod['nama'] ?></td>
                                        <td><?php echo $prod['warna'] ?></td>
                                        <td><?php echo $row['stok_awal'] ?></td>
                                        <td><?php echo $row['tujuan'] ?></td>
                                        <td><?php echo $row['jumlah'] ?></td>
                                        <td><?php echo $row['stok_akhir'] ?></td>
                                        <td><?php echo $row['keterangan'] ?></td>
                                        <td><?php echo $row['periode'] ?></td>
                                    </tr>

                                    <?php 
                                }
                                ?>
	</table>
</body>
</html>