<?php
	include '../config/koneksi.php';
	session_start();
	$data_user = mysql_fetch_array(mysql_query("SELECT * FROM login WHERE kode = '$_SESSION[username]' "));
?>
<html>
	<head>
		<title> Angha Comfort | Keuangan</title>
	</head>
		<body>
			<?php
				$notif = $_GET['notif'];
				if ($notif==1) {
					echo "
						<div class='alert alert-success' id='notif'>
							<button type='button' class='close' data-dismiss='alert'>×</button>
							<strong>Sukses ! ! !</strong> Data Profil berhasil di edit.
						</div>
					";
				}elseif($notif==2){
					echo "
						<div class='alert alert-success' id='notif'>
							<button type='button' class='close' data-dismiss='alert'>×</button>
							<strong>Sukses ! ! !</strong> Password Berhasil di Ubah.
						</div>
					";
				}
			?>
			<div class="row">
                <div class="col-lg-12">
                	<div class="panel panel-primary">
                		<div class="panel-heading">
                            Lihat Profil
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            	 <div class="col-lg-12">
                            	 	<table class="table table-striped table-hover">
                            	 		<tr>
                            	 			<th>Kode</th>
                            	 			<td><?php echo $data_user['kode']; ?></td>
                            	 		</tr>
                            	 		<tr>
                            	 			<th>Nama Lengkap</th>
                            	 			<td><?php echo $data_user['nama_lengkap']; ?></td>
                            	 		</tr>
                            	 		<tr>
                            	 			<th>Alamat</th>
                            	 			<td><?php echo $data_user['alamat']; ?></td>
                            	 		</tr>
                            	 		<tr>
                            	 			<th>Nomor Hp</th>
                            	 			<td><?php echo $data_user['nomor_hp']; ?></td>
                            	 		</tr>
                            	 		<tr>
                            	 			<th>Username</th>
                            	 			<td><?php echo $data_user['username']; ?></td>
                            	 		</tr>
                            	 		<tr>
                            	 			<th>Password</th>
                            	 			<td><?php echo $data_user['password']; ?></td>
                            	 		</tr>
                            	 		<tr>
                            	 			<th>Status Aktif</th>
                            	 			<td>
                            	 				<input type="radio" value="Y" disabled <?php if ($data_user['aktif']=="Y") {
                            	 					echo "checked";
                            	 				} ?> /> Aktif &nbsp;&nbsp;
                            	 				<input type="radio" value="N" disabled <?php if ($data_user['aktif']=="N") {
                            	 					echo "checked";
                            	 				} ?> /> Blokir
                            	 			</td>
                            	 		</tr>
                            	 	</table>
                            	 </div>
                            </div>
                        </div>
                	</div>
                </div>
            </div>
		</body>
</html>