<?php
	include '../config/koneksi.php';
	session_start();
	$result = mysql_fetch_array(mysql_query("SELECT * FROM login WHERE kode = '$_SESSION[username]' "));
?>
<html>
	<head>
		<title> Angha Comfort | Keuangan</title>
		<script type="text/javascript">
			function validasi() {
				var nama = document.getElementById('nama');
				var alamat = document.getElementById('alamat');
				var nomor = document.getElementById('nomor');
				var username = document.getElementById('username');

				if (nama.value=="") {
					alert("Maaf, Kolom nama lengkap harus diisi");
					nama.focus();
					return false;
				}else if(!isNaN(nama.value)){
					alert("Maaf, Kolom nama harus diisi huruf");
					nama.focus();
					return false;
				}else if(alamat.value==""){
					alert("Maaf, Kolom alamat harus diisi");
					alamat.focus();
					return false;
				}else if(nomor.value==""){
					alert("Maaf, Kolom nomor harus diisi");
					nomor.focus();
					return false;
				}else if(isNaN(nomor.value)){
					alert("Maaf, Kolom nomor harus diisi angka");
					nomor.focus();
					return false;
				}else if(username.value==""){
					alert("Maaf, Kolom username harus diisi");
					username.focus();
					return false;
				}else{
					return true;
				}
			}
		</script>
	</head>
		<body>
			<div class="row">
                <div class="col-lg-12">
                	<div class="panel panel-primary">
                		<div class="panel-heading">
                            Edit Profil
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            	 <div class="col-lg-12">
                            	 	<form role="form" action="module/profil/controls/proses_update_admin.php" method="POST" onsubmit="return validasi()">
                            	 		<div class="form-group">
                                            <label>Kode</label>
                                            <input type="text" name="kode" readonly value="<?php echo $result['kode']; ?>" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" name="nama_lengkap" id="nama" value="<?php echo $result['nama_lengkap']; ?>" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <input type="text" name="alamat" id="alamat" value="<?php echo $result['alamat']; ?>" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Hp</label>
                                            <input type="text" name="nomor_hp" id="nomor" value="<?php echo $result['nomor_hp']; ?>" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="username" id="username" value="<?php echo $result['username']; ?>" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input type="submit" name="go_edit" value="Edit Profil" class="btn btn-success" style="float:right;" />
                                        </div>
                            	 	</form>
                            	 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</body>
</html>