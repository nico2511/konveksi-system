<html>
	<head>
		<title> Angha Comfort | Keuangan</title>
		<script type="text/javascript">
			function validasi() {
				var pass_lama = document.getElementById('pass_lama');
				var pass_baru = document.getElementById('pass_baru');
				var konfirm = document.getElementById('konfirm');

				if (pass_lama.value=="") {
					alert("Maaf, kolom password lama harus diisi");
					pass_lama.focus();
					return false;
				}else if(pass_baru.value==""){
					alert("Maaf, kolom password baru harus diisi");
					pass_baru.focus();
					return false;
				}else if(konfirm.value==""){
					alert("Maaf, kolom konfirmasi harus diisi");
					konfirm.focus();
					return false;
				}else{
					return true;
				}
			}
		</script>
	</head>
		<body>
			<div class="row">
				<div class="col-lg-10">
                	<div class="panel panel-primary">
                		<div class="panel-heading">
                            Ganti Password
                        </div>
                        <div class="panel-body">
                        	<form role="form" action="module/profil/controls/proses_ganti_password.php" method="POST" onsubmit="return validasi()">
                        		<div class="form-group">
                                	<label>Password Lama</label>
                                    <input type="password" name="password_lama" id="pass_lama" placeholder="Masukan password lama anda" class="form-control" />
                                </div>
                                <div class="form-group">
                                	<label>Password Baru</label>
                                    <input type="password" name="password_baru" id="pass_baru" placeholder="Masukan password baru anda" class="form-control" />
                                </div>
                                <div class="form-group">
                                	<label>Konfirmasi</label>
                                    <input type="password" name="konfirmasi" id="konfirm" placeholder="konfirmasi password baru anda" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="submit" name="go_ganti" value="Ganti Password" class="btn btn-success" style="float:right;" />
                                </div>
                        	</form>
                        </div>
                    </div>
                </div>
			</div>
		</body>
</html>