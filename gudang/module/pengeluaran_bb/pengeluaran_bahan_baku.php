<?php
	include '../config/koneksi.php';
    $query = mysql_query("SELECT * FROM permintaan_bahan_baku JOIN pengeluaran_bahan_baku ON permintaan_bahan_baku.id_permintaan = pengeluaran_bahan_baku.id_permintaan JOIN notifikasi_permintaan_bb ON notifikasi_permintaan_bb.id_permintaan = permintaan_bahan_baku.id_permintaan JOIN bahan_baku ON bahan_baku.kode_bahan_baku = permintaan_bahan_baku.kode_bahan_baku WHERE notifikasi_permintaan_bb.status_notif = 'sudah approve' ");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Notifikasi Permintaan Bahan Baku</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Permintaan Bahan Baku</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
							<table id="example2" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Permintaan</th>
                                        <th>Tanggal Approve</th>
                                        <th>Nama Bahan Baku</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no=1;
                                        while($histori_data = mysql_fetch_array($query)){
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $histori_data['tanggal_permintaan']; ?></th>
                                        <td><?php echo $histori_data['tanggal_keluar']; ?></td>
                                        <td><?php echo $histori_data['nama_bahan_baku']; ?></td>
                                        <td><?php echo $histori_data['jumlah_notif']; ?></td>
                                        <td>
                                            <?php
                                                if($histori_data['status_notif']=="belum approve"){
                                                    echo "
                                                        <span class='label label-warning'>Belum Approve</span>
                                                    ";
                                                }else if($histori_data['status_notif']=="sudah approve"){
                                                    echo "
                                                        <span class='label label-success'>Sudah Approve</span>
                                                    ";
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="module/pengeluaran_bb/hapus_pengeluaran_bb.php?id_permintaan=<?php echo $histori_data['id_permintaan']; ?>" class="btn btn-sm btn-danger" onclick="return confirm('Apakah anda ingin menghapus data ini ?')">  <i class="fa fa-trash fa-fw"></i> Hapus</a>
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>