<?php
	include '../config/koneksi.php';
	$id_notifikasi= $_GET['id_notifikasi'];
	$detail = mysql_fetch_array(mysql_query("SELECT * FROM notifikasi_permintaan_bb INNER JOIN permintaan_bahan_baku ON notifikasi_permintaan_bb.id_permintaan = permintaan_bahan_baku.id_permintaan JOIN bahan_baku ON bahan_baku.kode_bahan_baku = permintaan_bahan_baku.kode_bahan_baku WHERE notifikasi_permintaan_bb.id_notifikasi = '$id_notifikasi' "));
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Permintaan Bahan Baku</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Detail Permintaan Bahan Baku</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Tanggal Pesan</label>
                                <input type="text" name="tgl" id="tgl" class="form-control" value="<?= $detail['tanggal_notif']; ?>" disabled/>
                            </div>
                            <div class="form-group">
                                <label>Nama Bahan Baku</label>
                                <input type="text" name="tgl" id="tgl" class="form-control" value="<?= $detail['nama_bahan_baku']; ?>" disabled/>
                            </div>
                            <div class="form-group">
                                <label>Jumlah Permintaan</label>
                                <input type="text" name="tgl" id="tgl" class="form-control" value="<?= $detail['jumlah_notif']; ?>" disabled/>
                            </div>
                            <div class="form-group">
                                <?php 
                                    if ($detail['status_notif']=="belum approve") {
                                        echo "
                                            <span class='btn btn-sm btn-warning'>Belum Approve</span>
                                        ";
                                    }else if($detail['status_notif']=="sudah approve"){
                                        echo "
                                            <span class='btn btn-sm btn-success'>Sudah Approve</span>
                                        ";
                                    }
                                ?>
                            </div>
                            <div id="txtHint"></div>
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <a href="?page=all_notifikasi" class="btn btn-danger">Tampilkan Semua permintaan</a>
                                <a href="#notif" class="btn btn-primary" data-toggle="modal">Keluarkan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal -->
    <div class="modal fade bs-example-modal-md" id="notif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog modal-md" style="margin-top:50px;" >
            <div class="modal-content">
                <div class="modal-header">                    
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-shopping-cart fa fw"></i>&nbsp;&nbsp;Keluarkan Bahan Baku</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  </div>
                  <div class="modal-body" >
                        <form role="form" action="module/pengeluaran_bb/proses/proses_pengeluaran_bb.php" method="POST">
                            <input type="hidden" name="id_permintaan" value="<?php echo $detail['id_permintaan']; ?>" />
                            <input type="hidden" name="ket_baca" value="belum dibaca" />
                            <input type="hidden" name="kode_bahan_baku" value="<?php echo $detail['kode_bahan_baku']; ?>" />
                            <div class="form-group">
                                <label>Tanggal Permintaan</label>
                                <input type="text" name="tanggal_pengeluaran" id="tgl_minta"  class="form-control" value="<?php echo $detail['tanggal_permintaan']; ?>" readonly />
                            </div>
                            <div class="form-group">
                                <label>Tanggal Pengeluaran</label>
                                <input type="text" name="tanggal_pengeluaran" id="tgl_keluar"  class="form-control" value="<?php echo date('d F Y'); ?>" readonly />
                            </div>
                            <div class="form-group">
                                <label>Nama Bahan Baku</label>
                                <input type="text" name="nama_bahan_baku" id="nama_bb"  class="form-control" value="<?php echo $detail['nama_bahan_baku']; ?>" readonly />
                            </div>
                            <div class="form-group">
                                <label>Jumlah Pengeluaran</label>
                                <input type="text" name="jumlah_pengeluaran" id="jml"  class="form-control" value="<?php echo $detail['jumlah_permintaan']; ?>"  />
                            </div>
                  </div>
                    <div class="modal-footer">
                            <button type="submit" name="go_keluar" value="keluar" class="btn btn-primary">
                                <i class="fa fa-check fa-fw"></i> Submit
                            </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
    <!-- endmodal -->