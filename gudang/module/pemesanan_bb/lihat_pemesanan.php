<?php
	include '../config/koneksi.php';
    $sql_no = mysql_fetch_array(mysql_query("SELECT MAX(kode_pembelian) as kode_pembelian from pembelian_bahan_baku"));
    $lastID = $sql_no['kode_pembelian'];
    $lastNoUrut = substr($lastID, 3, 9);
    $nextNoUrut = $lastNoUrut . 1;
    $nextID = "IN/"."YOVIS/".sprintf("%03s",$nextNoUrut);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Penerimaan Bahan Baku</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Data Penerimaan BB</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-striped table-sm">
								<thead>
                        			<tr>
                        				<th>No</th>
                        				<th>Tanggal Diterima</th>
                        				<th>Jenis Bahan</th>
										<th>Jumlah (KG)</th>
										<th>Jumlah (ROLL)</th>
										<th>Warna</th>
                        				<th>Supplier</th>
                        				<th>Rincian</th>
                        				<th>Status Diterima</th>
                        				<th>Masuk Gudang</th>
                        			</tr>
                        		</thead>
                        		<tbody>
                        			<?php
                        				$query = mysql_query("SELECT * FROM pemesanan_bahan_baku JOIN supplier ON pemesanan_bahan_baku.kode_supplier = supplier.kode_supplier JOIN bahan_baku ON bahan_baku.kode_bahan_baku = pemesanan_bahan_baku.kode_bahan_baku ORDER BY pemesanan_bahan_baku.kode_pemesanan DESC");
                        				$no = 1;
                        				while($data_pesan = mysql_fetch_array($query)){
                        			?>
                        			<tr>
                        				<td><?php echo $no; ?></td>
                        				<td><?php echo date('d F Y',strtotime($data_pesan['tanggal_pesan'])); ?></td>
                        				<td><?php echo $data_pesan['nama_bahan_baku']; ?></td>
										<td><?php echo $data_pesan['jumlah_pesan']; ?></td>
										<td><?php echo $data_pesan['roll_pesan']; ?></td>
										<td><?php echo $data_pesan['warna']; ?></td>
                        				<td><?php echo $data_pesan['nama_supplier']; ?></td>
                        				<td><a href="?page=detail_pemesanan&kode_pemesanan=<?php echo $data_pesan['kode_pemesanan']; ?>" >Detail...</a></td>
                        				<td><span class="btn btn-sm btn-success" title="<?php echo date('d F Y',strtotime($data_pesan['tanggal_pesan'])); ?>">Sudah dikirim</span></td>
                        				<td>
                        					<?php
                        						if($data_pesan['status_terima']=="belum diterima"){
                        					?>
                        					<a href="#detail" class="open-detail" data-tglp="<?php echo date('d-m-Y',strtotime($data_pesan['tanggal_pesan'])); ?>" data-jenis="<?php echo $data_pesan['nama_bahan_baku']; ?>" data-warna="<?php echo $data_pesan['warna']; ?>" data-qty="<?php echo $data_pesan['jumlah_pesan']; ?>" data-idp="<?php echo $data_pesan['kode_pemesanan']; ?>" data-toggle="modal" style="text-decoration:none;">
                        						<span class="btn btn-sm btn-danger">Belum Masuk</span>
                        					</a>
                        					<?php
                        						}else if($data_pesan['status_terima']=="sudah diterima"){
                        					?>
                        					<span class="btn btn-sm btn-success">Sudah Masuk</span>
                        					<?php
                        						}
                        					?>
                        				</td>
                        			</tr>
                        			<?php
                        				$no++;
                        				}
                        			?>
                        		</tbody>
                        	</table>
                        </div>
                    </div>
					
          <div class="col-sm-7">
            <h2>Data Bahan Baku Masuk Gudang</h2>
									</div>
					<div class="card">
                        <div class="card-body">
                            <table id="example22" class="table table-bordered table-striped table-sm">
								<thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Jenis Bahan</th>
										<th>Warna</th>
                                        <th>Jumlah Diterima (KG)</th>
                                        <th>Jumlah Masuk (KG)</th>
                                        <th>Sisa</th>
										<th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $sql = mysql_query("SELECT * FROM pembelian_bahan_baku JOIN pemesanan_bahan_baku ON pembelian_bahan_baku.kode_pemesanan = pemesanan_bahan_baku.kode_pemesanan JOIN bahan_baku ON bahan_baku.kode_bahan_baku = pemesanan_bahan_baku.kode_bahan_baku ORDER BY pembelian_bahan_baku.kode_pembelian DESC");
                                        $no = 1;
                                        while($data_beli = mysql_fetch_array($sql)){
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo date('d F Y',strtotime($data_beli['tanggal_beli'])); ?></td>
                                        <td><?php echo $data_beli['nama_bahan_baku']; ?></td>
										<td><?php echo $data_beli['warna']; ?></td>
                                        <td><?php echo $data_beli['jumlah_pesan']; ?></td>
                                        <td><?php echo $data_beli['total_barang']; ?></td>
                                        <td>
                                            <?php
                                                echo $data_beli['jumlah_pesan'] - $data_beli['total_barang'];
                                            ?>
                                        </td>
										<td><button class="btn btn-sm btn-<?php if($data_beli['status'] == 'Sukses'){ echo 'success'; }else{ echo 'warning'; }; ?>"><?php echo $data_beli['status']; ?></button></td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                        	</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal -->
    <div class="modal fade bs-example-modal-lg" id="detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-dialog modal-lg" style="margin-top:50px;" >
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt fa fw"></i>&nbsp;&nbsp;Input Penerimaan</h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				</div>
				<form action="module/pemesanan_bb/proses/update_status_penerimaan.php" method="POST">
				<div class="modal-body" >
				<table class="table table-bordered">
				
					<tr>
						<th width="250">INV Penerimaan</th>
						<td><input type="text" name="kode_pemesanan" id="id_pesan" class="form-control" readonly /></td>
					</tr>
					<tr>
						<th width="250">INV Masuk Bahan Baku</th>
						<td><input type="text" name="kode_pembelian" id="kd_beli" value="<?php echo $nextID; ?>" class="form-control" readonly /></td>
					</tr>
						<th width="250">Tanggal Diterima</th>
						<td><input type="text" name="tanggal_pesan" id="tgl_pesan" class="form-control" disabled /></td>
					</tr>
					<input type="hidden" name="tanggal_beli" id="tgl" value="<?php echo date('d-m-Y'); ?>" class="form-control" readonly />
					<tr>
						<th>Jenis Bahan Baku</th>
						<td><input type="text" name="nama_bahan_baku" disabled id="nama_bb" class="form-control" /></td>
					</tr>
					<tr>
						<th>Total Yang Diterima</th>
						<input type="hidden" name="valtot" id="valtot">
						<td><input type="text" name="total" id="jml" onkeyup="valJml();ValTot();" class="form-control" /></td>
					</tr>
				</table>
				</div>
				<div class="modal-footer">
				<button type="submit" name="go_beli" value="beli" class="btn btn-primary">
						<i class="fa fa-check fa-fw"></i>Submit
				</button>
				</form>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
			
    <!-- end modal -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    
    <script type="text/javascript">

        $(function(){

			$('[data-toggle="tooltip"]').tooltip('show');

            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
            echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
            unset($_SESSION['toastr']);
        }
        ?>          
    });
        
	$(document).on("click", ".open-detail",function(){
		var tglpesan    = $(this).data('tglp');
		var jenis       = $(this).data('jenis');
		var id          = $(this).data('idp');
		var warna          = $(this).data('warna');
		var qty          = $(this).data('qty');
		$(".modal-body #tgl_pesan").val(tglpesan);
		$(".modal-body #jml").val(qty);
		$(".modal-body #valtot").val(qty);
		$(".modal-body #nama_bb").val(jenis+" - "+warna);
		$(".modal-body #id_pesan").val(id);
	});

	function valJml() {
		var jml = document.getElementById('jml');
		if (isNaN(jml.value)) {
			alert("Maaf, kolom jumlah harus diisi angka");
			jml.focus();
			jml.value="";
		}
	}

	function ValTot() {
		var jml 	  = document.getElementById('jml').value;
		var jml_pesan = document.getElementById('valtot').value;
		
		if (jml > jml_pesan) {
			toastr.error('Pembelian tidak bisa melebihi data pesanan');
			// jml.focus();
			$("#jml").val(jml_pesan);
		}
	}
    </script>