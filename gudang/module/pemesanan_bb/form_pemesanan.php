<?php
    include '../config/koneksi.php';
    $sql_no = mysql_fetch_array(mysql_query("SELECT MAX(kode_pemesanan) as kode_pemesanan from pemesanan_bahan_baku"));
    $lastID = $sql_no['kode_pemesanan'];
    $lastNoUrut = substr($lastID, 3, 9);
    $nextNoUrut = $lastNoUrut + 1;
    $nextID = "INV/".date("Ymd")."/".sprintf("%03s",$nextNoUrut);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Supplier</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Supplier</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form role="form-horizontal" action="module/pemesanan_bb/proses/proses_pemesanan_bb.php" method="POST" onsubmit="return validasi()">
                                    <input type="hidden" name="status_terima" value="belum diterima" />
                                    <input type="hidden" name="keterangan" value="pembelian" />
                                    <div class="row">
                                    <div class="form-group col-2">
                                        <label>Kode Penerimaan</label>
                                        <input type="text" name="kode_pemesanan" id="kode" value="<?php echo $nextID; ?>" readonly class="form-control" />
                                    </div>
                                    <div class="form-group col-3">
                                        <label>Tanggal Penerimaan</label>
                                        <input type="date" name="tanggal_pemesanan" id="tgl" class="form-control" value="<?= date('Y-m-d') ?>"/>
                                    </div>
                                    <div class="form-group col-3">
                                    	<label>Nama Bahan Baku</label>
                                        <select name="kode_bahan_baku" id="nama_bb"  class="form-control" onchange="changeValue(this.value)">
                                            <option value="">----- Pilih bahan baku -----</option>
                                            <?php
                                                $query_bb = mysql_query("SELECT * FROM bahan_baku");
                                                $jsArray = "var bbb = new Array();\n";
                                                while($data_bb = mysql_fetch_array($query_bb)){
                                                    echo "
                                                        <option value='".$data_bb['kode_bahan_baku']."'>".$data_bb['nama_bahan_baku']." - ".$data_bb['warna']."</option>
                                                    ";
                                                    $jsArray .= "bbb['" . $data_bb['kode_bahan_baku'] . "'] = {desc:'".addslashes($data_bb['harga_satuan'])."'};\n";
                                                }
                                            ?>
                                            <script type="text/javascript">    
                                                <?php echo $jsArray; ?>  
                                                function changeValue(id){  
                                                    document.getElementById('harga').value = bbb[id].desc;
                                                };  
                                            </script> 
                                        </select>
                                    </div>
                                    <div class="form-group col-2">
                                        <label>No Seri</label>
                                        <input type="text" name="no_seri" id="no_seri" class="form-control" />
                                    </div>
                                    <div class="form-group col-2">
                                        <label>Harga Satuan</label>
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span><input type="text" name="harga_satuan" id="harga" readonly onkeyup="valJml();" class="form-control" />
                                            </div>
                                    </div>
                                    
</div>
                            		<div class="row">
                                    <div class="form-group col-2">
                                        <label>Jumlah Diterima (Kg)</label>
                                        <input type="text" name="jumlah" id="jml" onkeyup="valJml();" class="form-control" />
                                    </div>
                                    <div class="form-group col-2">
                                        <label>Roll</label>
                                        <input type="text" name="roll" id="roll" class="form-control" />
                                    </div>
                                    <div class="form-group col-2">
                                        <label>Total</label>
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span><input type="text" name="total" id="total" class="form-control" readonly />
                                            </div></div>
                                    <div class="form-group col-3">
                                        <label>Pilih Supplier</label>
                                        <select name="kode_supplier" id="supplier" class="form-control" onChange="showSupplier(this.value)">
                                            <option value="">----- Pilih Supplier -----</option>
                                            <?php
                                                $sql_supplier = mysql_query("SELECT * FROM supplier");
                                                while ($data_supplier = mysql_fetch_array($sql_supplier)) {
                                                    echo "
                                                        <option value='$data_supplier[kode_supplier]'>$data_supplier[nama_supplier]</option>
                                                    ";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-2">
                                        <label>Periode</label>
                                               <input type="text" id="periode" name="periode" class="form-control" value="<?php echo date('F Y'); ?>" readonly/> 
                                    </div>
                                    </div>
                                    <div id="txtHint"></div>
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <button type="submit" name="go_pesan" value="Pesan" class="btn btn-success" style="float:right;">
                                            Terima Bahan
                                        </button>
                                        <a href="?page=form_pemesanan" class="btn btn-danger" style="float:right; margin-right:10px;">Reset</a>
                                    </div>
                                    
                            	</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

        <script type="text/javascript">
            $(function() {
                $('#tgl').datepicker({
                    numberOfMonths: 2,
                    dateFormat: "dd MM yy",
                    showButtonPanel: true
                });
            });

            function validasi() {
                var tgl = document.getElementById('tgl');
                var nama_bb = document.getElementById('nama_bb');
                var jml = document.getElementById('jml');
                var harga = document.getElementById('harga');
                var supplier = document.getElementById('supplier');

                if (tgl.value=="") {
                    alert("Maaf, kolom tanggal harus diisi");
                    tgl.focus();
                    return false;
                }else if(nama_bb.value==""){
                    alert("Maaf, kolom nama bahan baku harus diisi");
                    nama_bb.focus();
                    return false;
                }else if(jml.value==""){
                    alert("Maaf, kolom jumlah harus diisi");
                    jml.focus();
                    return false;
                }else if(harga.value==""){
                    alert("Maaf, kolom harga harus diisi");
                    harga.focus();
                    return false;
                }else if(supplier.value==""){
                    alert("Maaf, kolom supplier harus diisi");
                    supplier.focus();
                    return false;
                }else{
                    return true;
                }
            }

            function valJml() {
                var jml = document.getElementById('jml');
                var harga = document.getElementById('harga');
                var total = document.getElementById('total');

                if (isNaN(jml.value)) {
                    alert("Maaf, kolom jumlah harus diisi angka");
                    jml.focus();
                    jml.value="";
                }else if(isNaN(harga.value)){
                    alert("Maaf, kolom harga harus diisi angka");
                    harga.focus();
                    harga.value="";
                }

                if(parseFloat(jml.value)){
                    total.value = jml.value * harga.value;
                }else if(parseFloat(harga.value)){
                    total.value = jml.value * harga.value;
                }else{
                    total.value="";
                }
            }
            
        </script>