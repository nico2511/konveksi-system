<?php
	include '../config/koneksi.php';
	$kode_pemesanan = $_GET['kode_pemesanan'];
	$result = mysql_fetch_array(mysql_query("SELECT * FROM pemesanan_bahan_baku JOIN supplier ON pemesanan_bahan_baku.kode_supplier = supplier.kode_supplier JOIN bahan_baku ON bahan_baku.kode_bahan_baku = pemesanan_bahan_baku.kode_bahan_baku WHERE pemesanan_bahan_baku.kode_pemesanan = '$kode_pemesanan' ORDER BY pemesanan_bahan_baku.tanggal_pesan DESC"))
?>
<!-- Content Header (Page header) -->
	<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Pemesanan Bahan Baku : <?= $result['kode_pemesanan'] ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">List Pesanan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        	<a href="?page=lihat_pemesanan" class="btn btn-danger"><i class="fa fa-arrow-left fa-fw"></i>&nbsp; Kembali</a><br /><br />
                        	<table class="table table-striped">
                        		<tr>
                        			<th width="200">Tanggal Pemesanan</th>
                        			<td width="50"> : </td>
                        			<td><?php echo $result['tanggal_pesan']; ?></td>
                        		</tr>
                        		<tr>
                        			<th>Nama Bahan Baku</th>
                        			<td> : </td>
                        			<td><?php echo $result['nama_bahan_baku']; ?></td>
                        		</tr>
								<tr>
                        			<th>Warna Bahan Baku</th>
                        			<td> : </td>
                        			<td><?php echo $result['warna']; ?></td>
                        		</tr>
                        		<tr>
                        			<th>Jumlah KG</th>
                        			<td> : </td>
                        			<td><?php echo $result['jumlah_pesan']; ?></td>
                        		</tr>
								<tr>
                        			<th>Jumlah Roll</th>
                        			<td> : </td>
                        			<td><?php echo $result['roll_pesan']; ?></td>
                        		</tr>
                        		<tr>
                        			<th>Harga Satuan</th>
                        			<td> : </td>
                        			<td><?php echo number_format($result['harga_satuan'],2,',','.'); ?></td>
                        		</tr>
                        		<tr>
                        			<th>Total Harga</th>
                        			<td> : </td>
                        			<td><?php echo number_format($result['total_harga'],2,',','.'); ?></td>
                        		</tr>
                        		<tr>
                        			<th>Status Terima</th>
                        			<td> : </td>
                        			<td><?php if($result['status_terima']=="belum diterima"){
										
									 ?><span class="btn btn-sm btn-danger">Belum diterima</span>
								<?php
                        						}elseif($result['status_terima']=="sudah diterima"){
                        					?>
                        					<span class="btn btn-sm btn-success">Sudah diterima</span>
                        					<?php
												}
												?>	
								</td>
                        		</tr>
                        		<tr>
                        			<th>Nama Supplier</th>
                        			<td> : </td>
                        			<td><?php echo $result['nama_supplier']; ?></td>
                        		</tr>
                        		<tr>
                        			<th>Alamat Supplier</th>
                        			<td> : </td>
                        			<td><?php echo $result['alamat_supplier']; ?></td>
                        		</tr>
                        		<tr>
                        			<th>Nomor Telp</th>
                        			<td> : </td>
                        			<td><?php echo $result['nomor_telp']; ?></td>
                        		</tr>
                        	</table>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</section>