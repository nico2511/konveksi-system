<?php
	include '../../../config/koneksi.php';
	$kode_pemesanan = $_GET['kode_pemesanan'];
	$result = mysql_fetch_array(mysql_query("SELECT * FROM pemesanan_bahan_baku JOIN supplier ON pemesanan_bahan_baku.kode_supplier = supplier.kode_supplier JOIN bahan_baku ON bahan_baku.kode_bahan_baku = pemesanan_bahan_baku.kode_bahan_baku WHERE pemesanan_bahan_baku.kode_pemesanan = '$kode_pemesanan' "));
	$company = mysql_fetch_array(mysql_query("SELECT * FROM pengaturan WHERE id = 1"));
?>
<html>
	<head>
		<title>Cetak Kartu Penerimaan | Gudang</title>
		<style type="text/css">
			.border-background {
				border: solid 1px #CCCCCC;
				width: 1000px;
				height: auto;
				margin: 0 auto;
			}
			.header {
				width: auto;
				height: auto;
			}
			.header h1 {
				text-align: center;
				font-family: Arial,Helvetica,sans-serif;
			}
			.body-pesan p {
				padding-left: 50px;
				font-family: Arial,Helvetica,sans-serif;
			}

			@media print {
				#printPageButton {
					display: none;
				}
			}
		</style>
	</head>
		<body onLoad="window.print()">
			<div class="border-background">
				<div class="header">
					<h1>FORM PENERIMAAN BAHAN BAKU</h1>
				</div> <hr style="width:900px;" />
				<div class="body-pesan">
					<p style="float:right; padding-right:180px; margin-top:-10px;">
						<b>Tanggal Pesan</b> : <?php echo $result['tanggal_pesan']; ?>
					</p>
					<p>
						<b>Kode Pemesanan</b> &nbsp;&nbsp;&nbsp;&nbsp;: 
						<?php echo $result['kode_pemesanan']; ?>
					</p>
					<p>
						<b>Nama Supplier</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
						<?php echo $result['nama_supplier']; ?>
					</p>
					<p>
						<b>Alamat Supplier</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
						<?php echo $result['alamat_supplier']; ?>
					</p>
					<p>
						<b>Nomor Telp</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
						<?php echo $result['nomor_telp']; ?>
					</p>
				</div>
				<table align="center" border="1" cellpadding="5" cellspacing="5" style="border-collapse:collapse; border:solid 1px #CCCCCC">
					<tr bgcolor="#F5F5F5">
						<th width="250">Nama Bahan Baku</th>
						<th width="80">Jumlah Kg</th>
						<th width="80">Jumlah Roll</th>
						<th width="200">Harga Satuan</th>
						<th width="250">Total Harga</th>
					</tr>
					<tr align="center">
						<td><?php echo $result['nama_bahan_baku']; ?></td>
						<td><?php echo $result['jumlah_pesan']; ?></td>
						<td><?php echo $result['roll_pesan']; ?></td>
						<td>Rp <?php echo number_format($result['harga_satuan'],2,',','.'); ?></td>
						<td>Rp <?php echo number_format($result['total_harga'],2,',','.'); ?></td>
					</tr>
				</table><br /> <hr style="width:900px;" /><br /><br /><br /><br /><br /><br />
				<p style="float:right; padding-right:120px; font-family: Arial,Helvetica,sans-serif;">TTD</p> <br /><br /><br />
				<p style="float:right; padding-right:60px; font-family: Arial,Helvetica,sans-serif;">Kepala Bag. Gudang</p><br /><br /><br /><hr style="width:900px;" />
				<p align="center" style="font-family: Arial,Helvetica,sans-serif; font-size:12px;">
					<i><?= $company['nama'] ?></i>
				</p>
				<div style="clear:both;"></div>
			</div>
			<br>
			<br>
			<div class="container">
				<div style="text-align: center;">
					<a href="../../?page=lihat_pemesanan" id="printPageButton" class="button" >List Pemesanan</a>
				</div>
			</div>
		</body>
</html>