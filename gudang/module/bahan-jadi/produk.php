<?php
include '../config/koneksi.php';
session_start();
$id   = mysql_real_escape_string($_GET['id']);
$data = mysql_fetch_array(mysql_query("SELECT * from produk where id = '$id'"));

?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Produk</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active">Produk</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <a href="#modal-tambah" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Tambah Produk Baru</a>
                        <br>
                        <br>
                        <table id="example1" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>

                                    <th colspan="5">
                                        <center>Jenis Produk</center>
                                    </th>
                                    <th colspan="7">
                                        <center>Ukuran Produk</center>
                                    </th>
                                    <th colspan="3">
                                        <center>Kelompok Produk</center>
                                    </th>
                                    <th colspan="3">
                                        <center>Detail Produk</center>
                                    </th>
                                </tr>
                                <tr>

                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Jenis</th>
                                    <th>Warna</th>
                                    <th>Kombinasi</th>
                                    <th>L</th>
                                    <th>XL</th>
                                    <th>XXL</th>
                                    <th>3L</th>
                                    <th>4L</th>
                                    <th>5L</th>
                                    <th>ALL SIZE</th>
                                    <th>SERI</th>
                                    <th>TDK SERI</th>
                                    <th>Total</th>
                                    <th>Lokasi</th>
                                    <th>Keterangan</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $brg = mysql_query("select * from produk order by id desc");
                                $no  = 1;
                                while ($b = mysql_fetch_array($brg)) {

                                    $dxl         = explode('-', $b['l']);
                                    $dxxl        = explode('-', $b['xl']);
                                    $dxxxl       = explode('-', $b['xxl']);
                                    $dx3l        = explode('-', $b['3l']);
                                    $dx4l        = explode('-', $b['4l']);
                                    $dx5l        = explode('-', $b['5l']);
                                    $dxall_size  = explode('-', $b['all_size']);
                                ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo $b['kode'] ?></td>
                                        <td><?php echo $b['jenis'] ?></td>
                                        <td><?php echo $b['warna'] ?></td>
                                        <td><?php echo $b['kombinasi'] ?></td>
                                        <td nowrap><?php echo (int)$dxl[0] . ' , Sisa : ' . (int)$dxl[1] ?></td>
                                        <td nowrap><?php echo (int)$dxxl[0] . ' , Sisa : ' . (int)$dxxl[1] ?></td>
                                        <td nowrap><?php echo (int)$dxxxl[0] . ' , Sisa : ' . (int)$dxxxl[1] ?></td>
                                        <td nowrap><?php echo (int)$dx3l[0] . ' , Sisa : ' . (int)$dx3l[1] ?></td>
                                        <td nowrap><?php echo (int)$dx4l[0] . ' , Sisa : ' . (int)$dx4l[1] ?></td>
                                        <td nowrap><?php echo (int)$dx5l[0] . ' , Sisa : ' . (int)$dx5l[1] ?></td>
                                        <td nowrap><?php echo (int)$dxall_size[0] . ' , Sisa : ' . (int)$dxall_size[1] ?></td>
                                        <td align="center"><?php if (is_int($b['stok'] / 12) && $b['stok'] != 0) {
                                                                echo '<i class="fas fa-check"></i>';
                                                            } ?></td>
                                        <td align="center"><?php if (!is_int($b['stok'] / 12) && $b['stok'] != 0) {
                                                                echo '<i class="fas fa-check"></i>';
                                                            } ?></td>
                                        <td><?php echo $b['stok'] ?></td>
                                        <td><?php echo $b['lokasi'] ?></td>
                                        <td><?php echo $b['keterangan'] ?></td>
                                        <td>
                                            <a href="?page=produk-masuk&id=<?php echo $b['id']; ?>" class="btn btn-success btn-sm"><i class="fas fa-sign-in-alt"></i></a>
                                            <a href="?page=produk-edit&id=<?php echo $b['id']; ?>" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#modal-hapus<?= $b['id'] ?>" data-toggle="modal" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a> &nbsp;
                                        </td>
                                    </tr>

                                    <div class="modal fade bs-example-modal-md" id="modal-hapus<?= $b['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-md" style="margin-top:50px;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    Apakah kamu yakin ingin menghapus <b><?= $b['nama'] ?> ?</b>
                                                    <form role="form-horizontal" action="module/bahan-jadi/proses/hapus-produk.php" method="POST">
                                                        <input type="hidden" name="id" readonly value="<?= $b['id'] ?>" id="id" class="form-control" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                    <button type="submit" name="go" value="hapus" class="btn btn-primary" style="float:right;">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- modal -->
<div id="modal-tambah" class="modal fade">
    <div class="modal-dialog modal-scroll">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Produk Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form role="form-horizontal" action="module/data_master/proses/tambah_produk.php" method="POST" onsubmit="return validasi()">
                    <div class="row">
                        <div class="form-group col-6">
                            <label>Jenis Produk</label>
                            <select class="form-control select2" data-placeholder="Jenis Produk" style="width: 100%;" name="nama">
                                <?php
                                $brg = mysql_query("select * from jenis_produk");
                                while ($b = mysql_fetch_array($brg)) {
                                ?>
                                    <option value="<?php echo $b['nama']; ?>"><?php echo $b['nama'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <label>Kode Produk</label>
                            <select class="form-control select2" data-placeholder="Kode Produk" style="width: 100%;" name="kode" readonly>
                                <?php
                                $brg = mysql_query("select * from jenis_produk");
                                while ($b = mysql_fetch_array($brg)) {
                                ?>
                                    <option value="<?php echo $b['kode']; ?>"><?php echo $b['kode'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label>Warna</label>
                            <select class="form-control select2" data-placeholder="Warna Produk" style="width: 100%;" name="warna">
                                <?php
                                $brg = mysql_query("select * from jenis_produk");
                                while ($b = mysql_fetch_array($brg)) {
                                ?>
                                    <option value="<?php echo $b['warna']; ?>"><?php echo $b['warna'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <label>Kombinasi</label>
                            <select class="form-control select2" data-placeholder="Warna Kombinasi Produk" style="width: 100%;" name="kombinasi">
                                <?php
                                $brg = mysql_query("select * from jenis_produk");
                                while ($b = mysql_fetch_array($brg)) {
                                ?>
                                    <option value="<?php echo $b['kombinasi']; ?>"><?php echo $b['kombinasi'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label>Stok</label>
                            <input name="stok" type="text" class="form-control" placeholder="Stok Produk ..">
                        </div>
                        <div class="form-group col-6">
                            <label>Lokasi</label>
                            <input name="lokasi" type="text" class="form-control" placeholder="Lokasi ..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input name="keterangan" type="text" class="form-control" placeholder="Keterangan ..">
                    </div>
                    <div class="form-group">
                        <label>Input By</label>
                        <input name="input_by" type="text" class="form-control" value="<?= $_SESSION['nama'] ?>" readonly>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-primary" value="Simpan">
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        <?php
        // toastr output & session reset
        session_start();
        if (isset($_SESSION['toastr'])) {
            echo 'toastr.' . $_SESSION['toastr']['type'] . '("' . $_SESSION['toastr']['message'] . '")';
            unset($_SESSION['toastr']);
        }
        ?>
    });

    function validasi() {
        // var kode_bb = document.getElementById('kode_bb');
        // var nama_bb = document.getElementById('nama_bb');
        // var harga   = document.getElementById('harga');
        // var warna   = document.getElementById('warna');
        // if(kode_bb.value==""){
        //     alert("maaf, kolom kode bahan baku harus diisi");
        //     kode_bb.focus();
        //     return false;
        // }else if(nama_bb.value==""){
        //     alert("maaf, kolom nama bahan baku harus diisi");
        //     nama_bb.focus();
        //     return false;
        // }else if(warna.value==""){
        //     alert("maaf, warna bahan baku harus diisi");
        //     warna.focus();
        //     return false;
        // }else if(harga.value==""){
        //     alert("maaf, kolom harga bahan baku harus diisi");
        //     harga.focus();
        //     return false;
        // }else{
        //     return true;
        // }
    }


    function valHarga() {
        var harga = document.getElementById('harga');
        if (isNaN(harga.value)) {
            alert("Maaf, kolom harga harus diisi angka");
            harga.focus();
            harga.value = "";
        }
    }
</script>