<?php
	include '../config/koneksi.php';
    $tanggal_cari       = $_POST['tanggal_cari'];
    $kode_bahan_baku    = $_POST['kode_bahan_baku'];
    $query = mysql_query("SELECT * FROM persediaan_bahan_baku LEFT JOIN bahan_baku ON persediaan_bahan_baku.kode_bahan_baku = bahan_baku.kode_bahan_baku WHERE persediaan_bahan_baku.kode_bahan_baku LIKE '%$kode_bahan_baku%' AND persediaan_bahan_baku.tanggal_persediaan LIKE '%$tanggal_cari%' ");
    $jum = 0;
    $jum_total = 0;
    $jum_harga = 0;
?>
<html>
	<head>
		<title> Angha Comfort | Gudang</title>
	    <script src="datepicker/jquery-1.3.2.js"></script>
        <script type="text/javascript" src="datepicker/ui.core.js"></script>
        <script type="text/javascript" src="datepicker/ui.datepicker.js"></script>
        <link href="datepicker/ui.all.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript">
            $(function() {
                $('#tgl').datepicker({
                    numberOfMonths: 3,
                    dateFormat: "MM yy",
                    showButtonPanel: true
                });
            });
        </script>
	</head>
		<body>
            <form role="form-inline" action="" method="POST">
                <table border="0">
                    <tr>
                        <td>
                            <div class="input-group">
                                <select name="kode_bahan_baku" class="form-control">
                                    <option value="">--- Pilih Bahan Baku ---</option>
                                    <?php
                                        $sql_bb = mysql_query("SELECT * FROM bahan_baku");
                                        while ($select_bb = mysql_fetch_array($sql_bb)) {
                                            echo "
                                                <option value='".$select_bb['kode_bahan_baku']."'>".$select_bb['nama_bahan_baku']."</option>
                                            ";
                                        }
                                    ?>
                                </select>
                            </div>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <div class="input-group">
                                  <input type="text" name="tanggal_cari" id="tgl" class="form-control" placeholder="cari Periode...">
                                  <span class="input-group-btn">
                                    <button type="submit" class="btn btn-danger" name="go_cari" value="cari">
                                        <span class="fa fa-search fa-fw"></span>
                                    </button>
                                  </span>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
			<div class="row">
				<div class="col-lg-12">
                	<div class="panel panel-primary">
                		<div class="panel-heading">
                            <i class="fa fa-list-alt fa-fw"></i> Data Persediaan Bahan Baku
                        </div>
                        <div class="panel-body">
                        	<table  class="table table-striped table-hover table-bordered">
                                <thead>
                                    <tr bgcolor="#F5F5F5">
                                        <th rowspan="2" style="text-align:center; vertical-align:middle;">No</th>
                                        <th rowspan="2" style="text-align:center; vertical-align:middle;">Tanggal</th>
                                        <th rowspan="2" style="text-align:center; vertical-align:middle;">Keterangan</th>
                                        <th colspan="3" style="text-align:center;">Pemasukan</th>
                                        <th colspan="3" style="text-align:center;">Pengeluaran</th>
                                        <th colspan="3" style="text-align:center;">Saldo Persediaan</th>
                                    </tr>
                                    <tr bgcolor="#F5F5F5">
                                        <th style="text-align:center;">Qty</th>
                                        <th style="text-align:center;">harga</th>
                                        <th style="text-align:center;">Total</th>
                                        <th style="text-align:center;">Qty</th>
                                        <th style="text-align:center;">harga</th>
                                        <th style="text-align:center;">Total</th>
                                        <th style="text-align:center;">Qty</th>
                                        <th style="text-align:center;">harga</th>
                                        <th style="text-align:center;">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        if(mysql_num_rows($query) > 0){
                                            while($data_bb = mysql_fetch_array($query)){
                                            if($_POST['go_cari']=="cari"){
                                            $jum = $jum + $data_bb['jumlah_masuk'] - $data_bb['jumlah_keluar'];
                                            $jum_total = $jum_total + $data_bb['total_masuk'];
                                            $jum_harga = $jum_harga + $data_bb['harga_masuk'];
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $data_bb['tanggal_persediaan']; ?></td>
                                        <td><?php echo ucwords($data_bb['keterangan'])." ".$data_bb['nama_bahan_baku']; ?></td>
                                        <td><?php echo $data_bb['jumlah_masuk']; ?></td>
                                        <td><?php echo number_format($data_bb['harga_masuk']); ?></td>
                                        <td><?php echo number_format($data_bb['total_masuk']); ?></td>
                                        <td><?php echo $data_bb['jumlah_keluar']; ?></td>
                                        <td><?php echo number_format($data_bb['harga_keluar']); ?></td>
                                        <td><?php echo number_format($data_bb['total_keluar']); ?></td>
                                        <td><?php echo $jum; ?></td>
                                        <td><?php echo number_format($jum_harga); ?></td>
                                        <td><?php echo number_format($jum_total); ?></td>
                                    </tr>
                                    <?php
                                        $no++;
                                            }
                                            }
                                        }else{
                                            echo "
                                                <tr>
                                                    <td colspan='12'><span class='label label-danger'>Tidak ada data yang ditampilkan...</span></td>
                                                </tr>
                                            ";
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
		</body>
</html>