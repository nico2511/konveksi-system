<?php
include '../config/koneksi.php';
$id   = mysql_real_escape_string($_GET['id']);
$data = mysql_fetch_array(mysql_query("SELECT * from produk where id = '$id'"));

?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Stok Masuk</h1>
                <h6><b><?= $data['kode'] . ' - ' . $data['jenis'] ?></b></h6>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active">Stok Masuk</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-md-11">
                <div class="card">
                    <div class="card-body">
                        <form role="form-horizontal" action="module/bahan-jadi/proses/masuk-produk.php" method="POST" onsubmit="return validasi_kirim()">
                            <div class="row">
                                <div class="form-group col-2">
                                    <label>Stok Sekarang</label>
                                    <input type="text" name="stok" id="stok" value="<?= $data['stok'] ?>" class="form-control" readonly />
                                </div>
                                <div class="form-group col-3">
                                    <label>Tanggal</label>
                                    <input type="date" name="tgl" value="<?= date('Y-m-d') ?>" id="tgl" class="form-control" />
                                </div>
                                <div class="form-group col-2">
                                    <label>Asal Barang</label>
                                    <input type="hidden" name="id" id="id" value="<?= $data['id'] ?>">
                                    <input type="text" name="asal_barang" id="asal_barang" class="form-control" />
                                </div>
                                <div class="form-group col-2">
                                    <label>Periode</label>
                                    <input type="text" id="periode" name="periode" class="form-control" value="<?php echo date('F Y'); ?>" readonly />
                                </div>
                                <div class="form-group col-2">
                                    <label>Input By</label>
                                    <input name="input_by" type="text" class="form-control" value="<?= $_SESSION['nama'] ?>" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-1">
                                    <label>L</label>
                                    <input type="text" name="l" id="l" class="form-control" />
                                </div>
                                <div class="form-group col-1">
                                    <label>XL</label>
                                    <input type="text" name="xl" id="xl" class="form-control" />
                                </div>
                                <div class="form-group col-1">
                                    <label>XXL</label>
                                    <input type="text" name="xxl" id="xxl" class="form-control" />
                                </div>
                                <div class="form-group col-1">
                                    <label>3L</label>
                                    <input type="text" name="3l" id="3l" class="form-control" />
                                </div>
                                <div class="form-group col-1">
                                    <label>4L</label>
                                    <input type="text" name="4l" id="4l" class="form-control" />
                                </div>
                                <div class="form-group col-1">
                                    <label>5L</label>
                                    <input type="text" name="5l" id="5l" class="form-control" />
                                </div>
                                <div class="form-group col-2">
                                    <label>ALL SIZE</label>
                                    <input type="text" name="all_size" id="all_size" class="form-control" />
                                </div>
                                <div class="form-group col-2">
                                    <label>Jumlah Masuk</label>
                                    <input type="text" name="jumlah_masuk" id="jumlah_masuk" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <input type="text" name="keterangan" id="keterangan" class="form-control" />
                            </div>
                            <div id="txtHint"></div>
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <button type="submit" name="go_pesan" value="Pesan" class="btn btn-success" style="float:right;">
                                    Submit
                                </button>
                                <a href="?page=produk" class="btn btn-danger" style="float:right; margin-right:10px;">Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function validasi_kirim() {
        var stok = document.getElementById('stok').value;
        var kg = document.getElementById('kg').value;

        var periode = document.getElementById('periode').value;
        if (kg == "") {
            toastr.error('Kg tidak boleh kosong');
            return false;
        } else if (kg == 0) {
            toastr.error('Kg tidak boleh kosong');
            return false;
        } else if (periode.value == "") {
            toastr.error('periode tidak boleh kosong');
            return false;
        } else {
            return true;
        }
    }

    function cek_stok() {
        var stok = document.getElementById('stok').value;
        var kg = document.getElementById('jumlah_masuk').value;

        if (kg > stok) {
            toastr.error('Stok tidak cukup');
            $("#jumlah_masuk").val(0);
        }
    }

    function valJml() {
        var jml = document.getElementById('jml');
        var harga = document.getElementById('harga');
        var total = document.getElementById('total');

        if (isNaN(jml.value)) {
            alert("Maaf, kolom jumlah harus diisi angka");
            jml.focus();
            jml.value = "";
        } else if (isNaN(harga.value)) {
            alert("Maaf, kolom harga harus diisi angka");
            harga.focus();
            harga.value = "";
        }

        if (parseFloat(jml.value)) {
            total.value = jml.value * harga.value;
        } else if (parseFloat(harga.value)) {
            total.value = jml.value * harga.value;
        } else {
            total.value = "";
        }
    }
</script>