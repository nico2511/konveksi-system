<?php
    include '../config/koneksi.php';
    $data_bb = mysql_fetch_array(mysql_query("SELECT * from bahan_baku where kode_bahan_baku = '$_GET[id]'"));
    
?>

<!-- Content Header (Page header) -->
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Keluar Stok <b><?= $data_bb['nama_bahan_baku'].' - '.$data_bb['warna'] ?></b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Keluar Stok</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <form role="form-horizontal" action="module/data_master/proses/keluar-bahan.php" method="POST" onsubmit="return validasi_kirim()">
                                <div class="form-group">
                                    <label>Stok</label>
                                    <input type="text" name="stok" id="stok" value="<?= $data_bb['kg'] ?>" class="form-control" readonly/>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input type="date" name="tgl" value="<?= date('Y-m-d') ?>" id="tgl"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Roll</label>
                                    <input type="hidden" name="kode" id="kode" value="<?= $data_bb['kode_bahan_baku'] ?>">
                                    <input type="text" name="roll" id="roll" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Kg</label>
                                    <input type="text" name="kg" id="kg" onchange="cek_stok()" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Tujuan</label>
                                    <input type="text" name="tujuan" id="tujuan" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Periode</label>
                                    <select class="form-control" id="periode" name="periode">
                                        <?php 
                                        $periode=mysql_query("SELECT * FROM periode ORDER BY id DESC limit 5");
                                        while($b=mysql_fetch_array($periode)){
                                            ?>	
                                            <option value="<?php echo $b['bulan']; ?>"><?php echo $b['bulan'] ?></option>
                                            <?php 
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div id="txtHint"></div>
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" name="go_pesan" value="Pesan" class="btn btn-success" style="float:right;">
                                        Kirim
                                    </button>
                                    <a href="?page=master_bahan_baku" class="btn btn-danger" style="float:right; margin-right:10px;">Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $('#tgl').datepicker({
                    numberOfMonths: 2,
                    dateFormat: "dd MM yy",
                    showButtonPanel: true
                });
            });

        function validasi_kirim() {
            var stok    = document.getElementById('stok').value;
            var kg      = document.getElementById('kg').value;
            
            var periode = document.getElementById('periode').value;
            if(kg == ""){
                toastr.error('Kg tidak boleh kosong');
                return false;
            }else if(kg == 0){
                toastr.error('Kg tidak boleh kosong');
                return false;
            }else if(periode.value==""){
                toastr.error('periode tidak boleh kosong');
                return false;
            }else{
                return true;
            }
        }

        function cek_stok(){
            var stok    = document.getElementById('stok').value;
            var kg      = document.getElementById('kg').value;
            
            if(Number(kg) > Number(stok)){
                toastr.error('Stok tidak cukup');
                $("#kg").val(0);
            }
        }

            function valJml() {
                var jml = document.getElementById('jml');
                var harga = document.getElementById('harga');
                var total = document.getElementById('total');

                if (isNaN(jml.value)) {
                    alert("Maaf, kolom jumlah harus diisi angka");
                    jml.focus();
                    jml.value="";
                }else if(isNaN(harga.value)){
                    alert("Maaf, kolom harga harus diisi angka");
                    harga.focus();
                    harga.value="";
                }

                if(parseFloat(jml.value)){
                    total.value = jml.value * harga.value;
                }else if(parseFloat(harga.value)){
                    total.value = jml.value * harga.value;
                }else{
                    total.value="";
                }
            }
        </script>