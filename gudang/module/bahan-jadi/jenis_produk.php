<?php
    include '../config/koneksi.php';
    
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Jenis Produk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Jenis Produk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="#modal-tambah" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Tambah Jenis Produk</a>
                            <br>
                            <br>
                            <table id="example1" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis Produk</th>
                                        <th>Kode</th>
                                        <th>Warna</th>
                                        <th>Kombinasi</th>
                                        <th>Stok</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                       
                                       $result = mysql_query("SELECT * FROM jenis_produk ORDER BY id DESC") or die(mysql_error());
                                       
                                        $no=1; //penomoran 
                                        while($rows=mysql_fetch_object($result)){
                                            $nama = $rows->nama;
                                            $x = mysql_fetch_array(mysql_query("SELECT sum(stok) as total from produk where jenis = '$nama'"));
                                            
                                                   ?>
                                               <tr>
                                                       <td><?php echo $no ?></td>
                                                       <td><?php echo $rows->nama;?></td>
                                                       <td><?php echo $rows->kode;?></td>
                                                       <td><?php echo $rows->warna;?></td>
                                                       <td><?php echo $rows->kombinasi;?></td>	
                                                       <td><?php echo $x['total'];?></td>
                                                       
                                               </tr>	                                               
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal -->
    <div class="modal fade bs-example-modal-md" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog modal-md" style="margin-top:50px;" >
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><span class="fa fa-plus fa-fw"></span>&nbsp;&nbsp;Tambah Jenis Produk</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body" >
                    <form role="form-horizontal" action="module/data_master/proses/tambah_jenis_produk.php" method="POST" onsubmit="return validasi()">
                        <div class="form-group">
                            <label>Jenis Produk</label>
                            <input type="text" name="jenis_produk" id="jenis_produk"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Kode Produk</label>
                            <input type="text" name="kode_produk" id="kode_produk"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Warna</label>
                            <input type="text" name="warna" id="warna"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Kombinasi</label>
                            <input type="text" name="kombinasi" id="kombinasi"  class="form-control" />
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="go_tambah" value="tambah" class="btn btn-primary" style="float:right;">Tambah</button>
                    <button type="reset" class="btn btn-danger" style="float:right; margin-right:10px;">Reset</button>
                </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function(){
            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
            echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
            unset($_SESSION['toastr']);
        }
        ?>          
    });
        
        function validasi() {
            var jenis = document.getElementById('jenis_produk');
            var kode = document.getElementById('kode_produk');
            
            if(jenis.value==""){
                toastr.error('Jenis Produk wajib diisi');
                jenis.focus();
                return false;
            }else if(kode.value==""){
                toastr.error('Kode Produk wajib diisi');
                kode.focus();
                return false;
            }else{
                return true;
            }
        }


        function valHarga() {
            var harga = document.getElementById('harga');
            if (isNaN(harga.value)) {
                alert("Maaf, kolom harga harus diisi angka");
                harga.focus();
                harga.value="";
            }
        }
    </script>