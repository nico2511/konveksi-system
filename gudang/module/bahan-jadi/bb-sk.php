<?php
    include '../config/koneksi.php';
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Bahan Baku Keluar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Bahan Keluar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Nama Bahan</th>
                                        <th>Kg Awal</th>
                                        <th>Roll Dikeluarkan</th>
                                        <th>Kg Dikeluarkan</th>
                                        <th>Kg Akhir</th>
                                        <th>Tujuan</th>
                                        <th>Periode</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if(isset($_GET['tgl'])){
                                    $no_nota=mysql_real_escape_string($_GET['tgl']);
                                    $brg=mysql_query("select * from bahan_keluar where tgl like '$no_nota' order by tgl desc");
                                }else{
                                    $brg=mysql_query("select * from bahan_keluar order by tgl desc");
                                }
                                $no=1;
                                while($b=mysql_fetch_array($brg)){
                                    $kod  = $b['nama'];
                                    $prod = mysql_fetch_array(mysql_query("SELECT * from bahan_baku where kode_bahan_baku = '$kod'"));
                                    ?>
                                    <tr>
                                        <td><?php echo $no++ ?></td>
                                        <td><?php echo date('d F Y',strtotime($b['tgl'])) ?></td>
                                        <td><?php echo $prod['nama_bahan_baku'].' - '.$prod['warna'] ?></td>
                                        <td><?php echo $b['kg_awal'] ?></td>
                                        <td><?php echo $b['roll'] ?></td>
                                        <td><?php echo $b['kg'] ?></td>
                                        <td><?php echo $b['kg_akhir'] ?></td>
                                        <td><?php echo $b['tujuan'] ?></td>
                                        <td><?php echo $b['periode'] ?></td>
                                        <td>
                                            <a href="edit_bahan_keluar.php?id=<?php echo $b['id']; ?>" class="btn btn-primary">Edit</a>
                                            <a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='hapus_bahan_keluar.php?id=<?php echo $b['id']; ?>&nama=<?php echo $b['nama'] ?>&periode=<?php echo $b['periode']; ?>' }" class="btn btn-danger">Hapus</a>
                                        </td>
                                    </tr>

                                    <div class="modal fade bs-example-modal-md" id="modal-hapus<?= $b['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                        <div class="modal-dialog modal-md" style="margin-top:50px;" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                </div>
                                                <div class="modal-body" >
                                                    Jika transaksi di return maka, catatan pembelian akan dihapus, Apakah kamu yakin ingin me-return pembelian ini ?</b>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                    <a class="btn btn-primary" style="float: right;" href="module/data_master/proses/return_pembelian.php?id=<?php echo $b['id']; ?>">Return</a>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php 
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal -->
    
    <!-- end modal -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function(){
            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
            echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
            unset($_SESSION['toastr']);
        }
        ?>          
    });
        
        function validasi() {
            var kode_bb = document.getElementById('kode_bb');
            var nama_bb = document.getElementById('nama_bb');
            var harga   = document.getElementById('harga');
            var warna   = document.getElementById('warna');
            if(kode_bb.value==""){
                alert("maaf, kolom kode bahan baku harus diisi");
                kode_bb.focus();
                return false;
            }else if(nama_bb.value==""){
                alert("maaf, kolom nama bahan baku harus diisi");
                nama_bb.focus();
                return false;
            }else if(warna.value==""){
                alert("maaf, warna bahan baku harus diisi");
                warna.focus();
                return false;
            }else if(harga.value==""){
                alert("maaf, kolom harga bahan baku harus diisi");
                harga.focus();
                return false;
            }else{
                return true;
            }
        }


        function valHarga() {
            var harga = document.getElementById('harga');
            if (isNaN(harga.value)) {
                alert("Maaf, kolom harga harus diisi angka");
                harga.focus();
                harga.value="";
            }
        }
    </script>