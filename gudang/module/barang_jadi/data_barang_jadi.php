<?php
	include '../config/koneksi.php';
?>
<html>
	<head>
		<title> Angha Comfort | Gudang</title>
		<script src="datatables/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		<script src="datatables/jquery.dataTables.js" type="text/javascript"></script>
    	<link href="datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    	<link href="datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    	<script type="text/javascript">
			$(document).ready(function(){
			    $('#data').DataTable();
			});
		</script>
	</head>
		<body>
			<div class="row">
				<div class="col-lg-12">
                	<div class="panel panel-primary">
                		<div class="panel-heading">
                            <i class="fa fa-cog fa-fw"></i> Data Barang Jadi
                        </div>
                        <div class="panel-body">
                        	<table id="data" class="table table-striped table-hover">
                        		<thead>
					        			<tr>
					        				<th>No</th>
					        				<th>Kode Barang</th>
					        				<th>Tgl Jadi</th>
					        				<th>Keterangan</th>
					        				<th>Jumlah</th>
					        				<th>Harga Satuan</th>
					        			</tr>
					        		</thead>
					        		<tbody>
					        			<?php
					        				$query = mysql_query("SELECT * FROM barang_jadi JOIN bahan_baku ON barang_jadi.kode_bahan_baku = bahan_baku.kode_bahan_baku WHERE barang_jadi.sent = 'Y' ORDER BY barang_jadi.tgl_jadi DESC");
					        				$no = 1;
					        				while($data = mysql_fetch_array($query)){
					        			?>
					        			<tr>
					        				<td><?php echo $no; ?></td>
					        				<td><?php echo $data['kode_barang_jadi']; ?></td>
					        				<td><?php echo $data['tgl_jadi']; ?></td>
					        				<td><?php echo $data['nama_bahan_baku']; ?></td>
					        				<td><?php echo $data['jumlah']; ?></td>
					        				<td><?php echo number_format($data['total'],2,',','.'); ?></td>
					        			</tr>
					        			<?php
					        				$no++;
					        				}
					        			?>
					        		</tbody>
                        	</table>
                        </div>
                    </div>
                </div>
            </div>
		</body>
</html>