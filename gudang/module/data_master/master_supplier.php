<?php
    include '../config/koneksi.php';
    $sql = mysql_fetch_array(mysql_query("SELECT MAX(kode_supplier) as kode_supplier from supplier"));
    $lastID = $sql['kode_supplier'];
    $lastNoUrut = substr($lastID, 3, 9);
    $nextNoUrut = $lastNoUrut + 1;
    $nextID = "SP".sprintf("%03s",$nextNoUrut);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Supplier</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Supplier</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <a href="#modal-tambah" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Tambah Supplier</a>
                            <br>
                            <br>
                            <table id="example1" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>Telp</th>
                                        <th>Email</th>
                                        <th class="notexport">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                        $sql_supplier = mysql_query("SELECT * FROM supplier");
                                        $no =1;
                                        while($data_supplier = mysql_fetch_array($sql_supplier)){
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $data_supplier['kode_supplier']; ?></td>
                                        <td><?php echo $data_supplier['nama_supplier']; ?></td>
                                        <td><?php echo $data_supplier['alamat_supplier']; ?></td>
                                        <td><?php echo $data_supplier['nomor_telp']; ?></td>
                                        <td><?php echo $data_supplier['email']; ?></td>
                                        <td>
                                            <a href="#alert-edit" class="open-edit btn btn-primary" data-toggle="modal" data-kodesupplier="<?php echo $data_supplier['kode_supplier']; ?>" data-namasupplier="<?php echo $data_supplier['nama_supplier']; ?>" data-alamatsupplier="<?php echo $data_supplier['alamat_supplier']; ?>" data-nomorhp="<?php echo $data_supplier['nomor_telp']; ?>" data-email="<?php echo $data_supplier['email']; ?>">Edit
                                            </a>&nbsp;
                                            <a href="#modal-hapus<?= $data_supplier['kode_supplier'] ?>" data-toggle="modal" class="btn btn-danger">Hapus</a> &nbsp;
                                            </a>
                                        </td>
                                    </tr>

                                    <div class="modal fade bs-example-modal-md" id="modal-hapus<?= $data_supplier['kode_supplier'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                        <div class="modal-dialog modal-md" style="margin-top:50px;" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                </div>
                                                <div class="modal-body" >
                                                    Apakah kamu yakin ingin menghapus supplier <b><?= $data_supplier['nama_supplier'] ?> ?</b>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                    <a class="btn btn-primary" style="float: right;" href="module/data_master/proses/hapus_supplier.php?kode_supplier=<?php echo $data_supplier['kode_supplier']; ?>">Hapus</a>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal -->
    <div class="modal fade bs-example-modal-md" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                  <div class="modal-dialog modal-md" style="margin-top:50px;" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel"><span class="fa fa-plus fa-fw"></span>&nbsp;&nbsp;Tambah Supplier</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      </div>
                      <div class="modal-body" >
                        	<form role="form" action="module/data_master/proses/tambah_supplier.php" method="POST" onsubmit="return validasi()">
                                <div class="form-group">
                                    <label>Kode Supplier</label>
                                    <input type="text" name="kode_supplier" id="kode" value="<?php echo $nextID; ?>" readonly  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Nama Supplier</label>
                                    <input type="text" name="nama_supplier" id="nama"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Alamat Supplier</label>
                                    <input type="text" name="alamat_supplier" id="alamat"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Nomor Telp</label>
                                    <input type="text" name="nomor_hp" id="nomor_hp" onkeyup="valNomor()"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" id="email"  class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" name="go_tambah" value="tambah" class="btn btn-primary" style="float:right;">
                                        Tambah
                                    </button>
                                    <button type="reset" class="btn btn-danger" style="float:right; margin-right:10px;">Reset
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
           	
            <!-- alert Edit Supplier -->
            <div class="modal fade bs-example-modal-md" id="alert-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog modal-md" style="margin-top:50px;" >
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit Data Supplier</h4>
                  </div>
                  <div class="modal-body" >
                    <form action="module/data_master/proses/edit_supplier.php" method="POST">
                        <div class="form-group">
                            <label>Kode Supplier</label>
                            <input type="text" name="kode_supplier" id="edit_kode" readonly class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Nama Supplier</label>
                            <input type="text" name="nama_supplier" id="edit_nama"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Alamat Supplier</label>
                            <input type="text" name="alamat_supplier" id="edit_alamat"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Nomor Telp</label>
                            <input type="text" name="nomor_hp" id="edit_nomor_hp" onkeyup="valNomor()"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" id="edit_email" class="form-control" />
                        </div>
                  </div>
                  <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                        <button type="submit" name="go_edit" value="edit" class="btn btn-primary">Edit
                        </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- End alert Edit Supplier -->
    <!-- end modal -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    
    <script type="text/javascript">

        $(function(){
            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
            echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
            unset($_SESSION['toastr']);
        }
        ?>          
    });
        
            $(document).on("click", ".open-edit", function(){
                var kode_supplier = $(this).data('kodesupplier');
                var nama_supplier = $(this).data('namasupplier');
                var alamat_supplier = $(this).data('alamatsupplier');
                var nomor_hp = $(this).data('nomorhp');
                var email = $(this).data('email');

                $(".modal-body #edit_kode").val( kode_supplier );
                $(".modal-body #edit_nama").val( nama_supplier );
                $(".modal-body #edit_alamat").val( alamat_supplier );
                $(".modal-body #edit_nomor_hp").val( nomor_hp );
                $(".modal-body #edit_email").val( email );
            });

			function validasi() {
				var nama = document.getElementById('nama');
				var alamat = document.getElementById('alamat');
				var nomor_hp = document.getElementById('nomor_hp');
                var email = document.getElementById('email');

				if (nama.value=="") {
					alert("Maaf, kolom nama supplier harus diisi");
					nama.focus();
					return false;
				}else if(alamat.value==""){
					alert("Maaf, kolom alamat supplier harus diisi");
					alamat.focus();
					return false;
				}else if(nomor_hp.value==""){
					alert("Maaf, kolom nomor hp harus diisi");
					nomor_hp.focus();
					return false;
				}else if(nomor_hp > 13){
					alert("Maaf, kolom nomor hp maksimal harus 12 digit");
					nomor_hp.focus();
					return false;
				}else if(isNaN(nomor_hp.value)){
					alert("Maaf, kolom nomor hp harus diisi angka");
					nomor_hp.focus();
					return false;
				}else if(email.value==""){
                    alert("Maaf, kolom email harus diisi");
                    email.focus();
                    return false;
                }else{
					return true;
				}
			}

            function valNomor() {
                var nomor = document.getElementById('nomor_hp');
                if (isNaN(nomor.value)) {
                    alert("Maaf, nomor hp harus diisi angka");
                    nomor.focus();
                    nomor.value="";
                }
            }
    </script>