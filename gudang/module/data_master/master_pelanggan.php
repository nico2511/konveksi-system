<?php
    include '../config/koneksi.php';
    $sql = mysql_fetch_array(mysql_query("SELECT MAX(kode_pelanggan) AS kode_pelanggan FROM pelanggan"));
    $lastID = $sql['kode_pelanggan'];
    $lastNoUrut = substr($lastID, 3, 9);
    $nextNoUrut = $lastNoUrut + 1;
    $nextID = "PL".sprintf("%03s",$nextNoUrut);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pelanggan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Pelanggan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="#modal-tambah" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Tambah Pelanggan</a>
                            <br>
                            <br>
                            <table id="example1" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Nomor Hp</th>
                                        <th>Alamat</th>
                                        <th>Keterangan</th>
                                        <th class="notexport">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                        $sql = mysql_query("SELECT * FROM konsumen");
                                        $no =1;
                                        while($data = mysql_fetch_array($sql)){
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $data['nama']; ?></td>
                                        <td><?php echo $data['tlp']; ?></td>
                                        <td><?php echo $data['alamat']; ?></td>
                                        <td><?php echo $data['keterangan']; ?></td>
                                        <td>
                                            <a href="#alert-edit" class="open-edit btn btn-primary" data-toggle="modal" data-kodepelanggan="<?php echo $data['id']; ?>" data-namapelanggan="<?php echo $data['nama']; ?>" data-alamatpelanggan="<?php echo $data['alamat']; ?>" data-nomorhp="<?php echo $data['tlp']; ?>" data-keterangan="<?php echo $data['keterangan']; ?>" >Edit
                                            </a>
                                            <a href="#modal-hapus<?= $data['id'] ?>" data-toggle="modal" class="btn btn-danger">Hapus</a>
                                        </td>
                                    </tr>

                                    <div class="modal fade bs-example-modal-md" id="modal-hapus<?= $data['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                        <div class="modal-dialog modal-md" style="margin-top:50px;" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                </div>
                                                <div class="modal-body" >
                                                    Apakah kamu yakin ingin menghapus pelanggan <b><?= $data['nama'] ?> ?</b>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                    <a class="btn btn-primary" style="float: right;" href="module/data_master/proses/hapus_pelanggan.php?id_pelanggan=<?php echo $data['id']; ?>">Hapus</a>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal -->
    <div class="modal fade bs-example-modal-md" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                  <div class="modal-dialog modal-md" style="margin-top:50px;" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel"><span class="fa fa-plus fa-fw"></span>&nbsp;&nbsp;Tambah Pelanggan</h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      </div>
                      <div class="modal-body">
                        	<form action="module/data_master/proses/tambah_pelanggan.php" method="POST" onsubmit="return validation()">
                                <div class="form-group">
                                    <label>Nama Pelanggan</label>
                                    <input type="text" name="nama_pelanggan" id="nama" class="form-control" required/>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" name="alamat_pelanggan" id="alamat" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label>Nomor Hp</label>
                                    <input type="text" name="nomor_hp" id="no_hp" onkeyup="valNomor()" class="form-control" required />
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <input type="text" name="keterangan" id="keterangan" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" name="go_tambah" value="tambah" class="btn btn-primary" style="float:right;">Tambah
                                    </button>
                                    <button type="reset" class="btn btn-danger" style="float:right; margin-right:10px;">Reset
                                    </button>
                                </div>
                        	</form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade bs-example-modal-md" id="alert-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog modal-md" style="margin-top:50px;" >
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><span class="fa fa-pencil"></span>&nbsp;&nbsp;Edit Data Pelanggan</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  </div>
                  <div class="modal-body" >
                    <form action="module/data_master/proses/edit_pelanggan.php" method="POST">
                        <input type="hidden" name="kode_pelanggan" id="edit_kode" readonly class="form-control" />
                        <div class="form-group">
                            <label>Nama Pelanggan</label>
                            <input type="text" name="nama_pelanggan" id="edit_nama"  class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Alamat Pelanggan</label>
                            <input type="text" name="alamat_pelanggan" id="edit_alamat"  class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Nomor HP</label>
                            <input type="text" name="nomor_hp" id="edit_nomor_hp" onkeyup="valNomor()"  class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" name="keterangan" id="edit_keterangan" class="form-control" />
                        </div>
                  </div>
                  <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                        <button type="submit" name="go_edit" value="edit" class="btn btn-primary">Edit
                        </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
    <!-- end modal -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function(){
            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
            echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
            unset($_SESSION['toastr']);
        }
        ?>          
    });
        
            $(document).on("click", ".open-edit", function(){
                var kode = $(this).data('kodepelanggan');
                var nama = $(this).data('namapelanggan');
                var alamat = $(this).data('alamatpelanggan');
                var nomor_hp = $(this).data('nomorhp');
                var keterangan = $(this).data('keterangan');

                $(".modal-body #edit_kode").val(kode);
                $(".modal-body #edit_nama").val(nama);
                $(".modal-body #edit_alamat").val(alamat);
                $(".modal-body #edit_nomor_hp").val(nomor_hp);
                $(".modal-body #edit_keterangan").val(nomor_hp);
            });

            function validation () {
                var nama = document.getElementById('nama');
                var alamat = document.getElementById('alamat');
                var no_hp = document.getElementById('no_hp');

                if (nama.value=="") {
                    alert("Maaf, kolom nama harus diisi");
                    nama.focus();
                    return false;
                }else if(alamat.value==""){
                    alert("Maaf, kolom alamat harus diisi");
                    alamat.focus();
                    return false;
                }else if(no_hp.value==""){
                    alert("Maaf, kolom nama harus diisi");
                    no_hp.focus();
                    return false;
                }else{
                    return true;
                }
            }

            function valNomor() {
                var no_hp = document.getElementById('no_hp');
                if (isNaN(no_hp.value)) {
                    alert("Maaf, nomor hp harus diisi angka");
                    no_hp.focus();
                    no_hp.value="";
                }
            }
    </script>