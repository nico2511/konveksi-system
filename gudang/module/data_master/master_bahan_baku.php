<?php
    include '../config/koneksi.php';
    $sql = mysql_fetch_array(mysql_query("SELECT MAX(kode_bahan_baku) as kode_bahan_baku from bahan_baku"));
    $lastID = $sql['kode_bahan_baku'];
    $lastNoUrut = substr($lastID, 3, 9);
    $nextNoUrut = $lastNoUrut + 1;
    $nextID = "BB".sprintf("%03s",$nextNoUrut);
?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Bahan Baku</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Bahan Baku</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="#modal-tambah" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Tambah Bahan Baku</a>
                            <table id="example2" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Bahan Baku</th>
                                        <th>Nama Bahan Baku</th>
                                        <th>Warna</th>
                                        <th>Harga Satuan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $query = mysql_query("SELECT * FROM bahan_baku");
                                        $no =1;
                                        while($data_bb = mysql_fetch_array($query)){
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $data_bb['kode_bahan_baku']; ?></td>
                                        <td><?php echo $data_bb['nama_bahan_baku']; ?></td>
                                        <td><?php echo $data_bb['warna']; ?></td>
                                        <td><?php echo number_format($data_bb['harga_satuan'],2,',','.'); ?></td>
                                        <td>
                                            <a href="#modal-edit<?= $data_bb['kode_bahan_baku'] ?>" data-toggle="modal" class="btn btn-primary">Edit</a> &nbsp;
                                            <a href="#modal-hapus<?= $data_bb['kode_bahan_baku'] ?>" data-toggle="modal" class="btn btn-danger">Hapus</a> &nbsp;
                                        </td>
                                    </tr>
                                    <div class="modal fade bs-example-modal-md" id="modal-edit<?= $data_bb['kode_bahan_baku'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                        <div class="modal-dialog modal-md" style="margin-top:50px;" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel"><span class="fa fa-plus fa-fw"></span>&nbsp;&nbsp;Edit <?= $data_bb['nama_bahan_baku'] ?> - <?= $data_bb['warna'] ?></h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                </div>
                                                <div class="modal-body" >
                                                    <form role="form-horizontal" action="module/data_master/proses/edit_bahan_baku.php" method="POST" onsubmit="return validasi()">
                                                        <div class="form-group">
                                                            <label>Kode Bahan Baku</label>
                                                            <input type="text" name="kode_bahan_baku" readonly value="<?= $data_bb['kode_bahan_baku'] ?>" id="kode_bb"  class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Nama Bahan Baku</label>
                                                            <input type="text" name="nama_bahan_baku" id="nama_bb" value="<?= $data_bb['nama_bahan_baku'] ?>" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Warna</label>
                                                            <input type="text" name="warna" id="warna" value="<?= $data_bb['warna'] ?>" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Roll</label>
                                                            <input type="text" name="rol" id="rol" value="<?= $data_bb['rol'] ?>" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>KG</label>
                                                            <input type="text" name="kg" id="kg" value="<?= $data_bb['kg'] ?>" class="form-control" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Harga</label>
                                                            <input type="text" name="harga_satuan" id="harga" onkeyup="valHarga();" value="<?= $data_bb['harga_satuan'] ?>" class="form-control" />
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                    <button type="submit" name="go_edit" value="edit" class="btn btn-primary" style="float:right;">Edit</button>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade bs-example-modal-md" id="modal-hapus<?= $data_bb['kode_bahan_baku'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                                        <div class="modal-dialog modal-md" style="margin-top:50px;" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                </div>
                                                <div class="modal-body" >
                                                    Apakah kamu yakin ingin menghapus <b><?= $data_bb['nama_bahan_baku'] ?> ?</b>
                                                    <form role="form-horizontal" action="module/data_master/proses/hapus_bahan_baku.php" method="POST">
                                                            <input type="hidden" name="kode_bahan_baku" readonly value="<?= $data_bb['kode_bahan_baku'] ?>" id="kode_bb"  class="form-control" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="float:right; margin-right:10px;">Batal</button>
                                                    <button type="submit" name="go" value="hapus" class="btn btn-primary" style="float:right;">Hapus</button>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        $no++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- modal -->
    <div class="modal fade bs-example-modal-md" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog modal-md" style="margin-top:50px;" >
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><span class="fa fa-plus fa-fw"></span>&nbsp;&nbsp;Tambah Bahan Baku</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body" >
                    <form role="form-horizontal" action="module/data_master/proses/tambah_bahan_baku.php" method="POST" onsubmit="return validasi()">
                        <div class="form-group">
                            <label>Kode Bahan Baku</label>
                            <input type="text" name="kode_bahan_baku" readonly value="<?php echo $nextID; ?>" id="kode_bb"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Nama Bahan Baku</label>
                            <input type="text" name="nama_bahan_baku" id="nama_bb"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Warna</label>
                            <input type="text" name="warna" id="warna"  class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input type="text" name="harga_satuan" id="harga" onkeyup="valHarga();" class="form-control" />
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="go_tambah" value="tambah" class="btn btn-primary" style="float:right;">Tambah</button>
                    <button type="reset" class="btn btn-danger" style="float:right; margin-right:10px;">Reset</button>
                </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(function(){
            <?php
                // toastr output & session reset
            session_start();
            if(isset($_SESSION['toastr'])){
            echo 'toastr.'.$_SESSION['toastr']['type'].'("'.$_SESSION['toastr']['message'].'")';
            unset($_SESSION['toastr']);
        }
        ?>          
    });
        
        function validasi() {
            var kode_bb = document.getElementById('kode_bb');
            var nama_bb = document.getElementById('nama_bb');
            var harga   = document.getElementById('harga');
            var warna   = document.getElementById('warna');
            if(kode_bb.value==""){
                alert("maaf, kolom kode bahan baku harus diisi");
                kode_bb.focus();
                return false;
            }else if(nama_bb.value==""){
                alert("maaf, kolom nama bahan baku harus diisi");
                nama_bb.focus();
                return false;
            }else if(warna.value==""){
                alert("maaf, warna bahan baku harus diisi");
                warna.focus();
                return false;
            }else if(harga.value==""){
                alert("maaf, kolom harga bahan baku harus diisi");
                harga.focus();
                return false;
            }else{
                return true;
            }
        }

        function valHarga() {
            var harga = document.getElementById('harga');
            if (isNaN(harga.value)) {
                alert("Maaf, kolom harga harus diisi angka");
                harga.focus();
                harga.value="";
            }
        }
    </script>