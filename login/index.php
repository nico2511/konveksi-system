<!DOCTYPE html>
<html>
<head>
	<title>LOGIN - YOVIS GROUP</title>
	<!-- Site favicon -->
	<link rel="shortcut icon" href="https://andriafesyenindonesiatekstil.co.id/wp-content/uploads/2021/08/favicon-32x32-3.png" type="image/x-icon" />
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,600,700" rel="stylesheet">
	<!-- Icon Font -->
	<link rel="stylesheet" href="fonts/ionicons/css/ionicons.css">
	<!-- Text Font -->
	<link rel="stylesheet" href="fonts/font.css">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Normal style CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Normal media CSS -->
	<link rel="stylesheet" href="css/media.css">
</head>
<body>

	<!-- Header start -->
	
	<!-- Header end -->
	<main class="cd-main">
		<section class="cd-section index visible ">
			<div class="cd-content style1">
				<div class="login-box d-md-flex align-items-center">
					<h1 class="title">YOVIS GROUP</h1>
					<h3 class="subtitle">The Best Choice For Business Partner</h3>
					<div class="login-form-box">
						<div class="login-form-slider">
							<!-- login slide start -->
							<div class="login-slide slide login-style1">
							<form action="proses_login.php" method='POST'>
									<div class="form-group">
										<label class="label">Username</label>
										<input required type="text" name="username" class="form-control" placeholder="Username" />
									</div>
									<div class="form-group">
										<label class="label">Password</label>
										<input required type="password" name="password" class="form-control" placeholder="Password" />
									</div>
									<div class="form-group">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="customCheck1">
											<label class="custom-control-label" for="customCheck1">Remember me</label>
										</div>
									</div>
									<div class="form-group">
									<input type="submit" name="go_login" class="submit" value="Login" />
									</div>
								</form>
								
								
								
							</div>
							<!-- login slide end -->
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<div id="cd-loading-bar" data-scale="1"></div>
	<!-- JS File -->
	<script src="js/modernizr.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script src="js/velocity.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</body>
</html>